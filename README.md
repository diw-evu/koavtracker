# #KoaVTracker

[![Latest Release](https://gitlab.com/diw-evu/koavtracker/-/badges/release.svg)](https://gitlab.com/diw-evu/koavtracker/-/releases) [![pipeline status](https://gitlab.com/diw-evu/koavtracker/badges/main/pipeline.svg)](https://gitlab.com/diw-evu/koavtracker/-/commits/main)

## Energie-Ziele im Koalitionsvertrag - und wo wir heute stehen

In ihrem [Koalitionsvertrag](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) vom 24.11.2021 haben sich die Ampel-Parteien diverse quantitative Ziele im Energiebereich gesetzt. Hier stellen wir einige dieser Ziele, die meist für das Jahr 2030 spezifiziert sind, grafisch dar und vergleichen sie regelmäßig mit dem aktuell tatsächlich erreichten Stand. Dabei konzentrieren wir uns zunächst auf Ziele im Bereich des Ausbaus erneuerbarer Energien, der Elektromobilität und der Elektrolyse. Für diese Indikatoren sind teils monatlich, teils aber auch nur jährlich aktualisierte Daten verfügbar. Aus rein illustrativen Gründen wird jeweils ein linearer Verlauf zwischen dem heutigen Stand und dem Zieljahr (meist Dezember 2030) dargestellt. Die genauen Pfade wurden im Koalitionsvertrag nicht spezifiert. Später werden wir ggf. weitere Indikatoren oder Zeitverläufe ergänzen.

## Über uns

Diese Seite wurde vom Forschungsbereich [Transformation der Energiewirtschaft](https://twitter.com/transenerecon) in der Abteilung [Energie, Verkehr, Umwelt](https://www.diw.de/de/diw_01.c.604205.de/abteilung_energie__verkehr__umwelt.html) am [DIW Berlin](https://www.diw.de/de) im Kontext des Kopernikus-Projekts [Ariadne](https://ariadneprojekt.de/) erstellt. Ansprechpartner im Fall von Rückfragen ist [Wolf-Peter Schill](https://www.diw.de/cv/de/wschill).

Alle Darstellungen und Erklärungen zu den Daten finden Sie auf der [Hauptseite des Projekts](https://diw-evu.gitlab.io/koavtracker/).
