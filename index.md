---
description: Der KoaVTracker visualisiert die Ziele und Fortschritte der Ampelkoalition für ausgewählte Bereiche der Energiewende.

robots: "ynztXlV7z-5AojzVIUCE9P5c_ogMFNCkebMRmOPMFC4"
hide:
  - navigation
  - toc
---

# #KoaVTracker

!!! danger "Neu in Version 0.5.3"
    
	Der KoavTracker umfasst nun auch Daten zu Wärmepumpen, zu monatlichen Neuzulassungen von Elektro-Pkw und zum Verhältnis vom Elektro-Pkw und Ladepunkten. Außerdem  enthält er jetzt ausgewählte Vergleichsdaten aus der [Szenarienanalyse](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/).
	
	
## Energie-Ziele im Koalitionsvertrag - und wo wir heute stehen

In ihrem [Koalitionsvertrag](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) vom 24.11.2021 haben sich die Ampel-Parteien diverse quantitative Ziele im Energiebereich gesetzt. Hier stellen wir diese Ziele, die meist für das Jahr 2030 spezifiziert sind, grafisch dar und vergleichen sie regelmäßig mit dem aktuell tatsächlich erreichten Stand. Dabei konzentrieren wir uns zunächst auf Ziele im Bereich des Ausbaus erneuerbarer Energien, der Elektromobilität und der Elektrolyse. Für diese Indikatoren sind teils monatlich, teils aber auch nur jährlich aktualisierte Daten verfügbar. Aus rein illustrativen Gründen wird jeweils ein linearer Verlauf zwischen dem heutigen Stand und dem Zieljahr (meist Dezember 2030) dargestellt. Die genauen Pfade wurden im Koalitionsvertrag nicht spezifiert. Für die meisten Indikatoren stellen wir auch Projektionen aktueller Trends dar. 

Die Ampel-Ziele und bisherigen Verläufe können bei einigen Indikatoren auch mit Ergebnissen der [Ariadne-Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) verglichen werden. Dabei handelt es sich um Szenarienanalysen, die im vom BMBF geförderten Kopernikus-Projekt [Ariadne](https://ariadneprojekt.de/) durchgeführt wurden und unterschiedliche Wege zur Klimaneutralität Deutschlands im Jahr 2045 aufzeigen. Wir stellen dabei sowohl die von allen Ariadne-Szenarien aufgespannte Korridore dar, als auch die Ergebnisse des jeweiligen Ariadne-Leitmodells im sogenannten *Technologiemix-Szenario* (8Gt_Bal). Bei den Ariadne-Szenarien sind Daten grundsätzlich nur in Fünfjahresschritten verfügbar (2020, 2025, 2030, ...). Zwischen diesen Stützjahren haben wir linear interpoliert. Die Daten der Ariadne-Szenarien liegen bis zum Jahr 2045 vor und erlauben somit einen längerfristigen Blick auf plausible Entwicklungen bei den jeweiligen Indikatoren. Weitere Visualisierungen sowie Daten zu allen Ariadne-Szenarien stehen auf externen Webseiten im [Pathfinder](https://pathfinder.ariadneprojekt.de/) sowie im [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne) zur Verfügung. Später werden wir ggf. noch weitere Indikatoren oder Zeitverläufe ergänzen.

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Über uns

Diese Seite wurde von [Adeline Guéret](https://www.diw.de/cv/de/agueret), [Alexander Roth](https://www.diw.de/cv/de/aroth) und [Wolf-Peter Schill](https://www.diw.de/cv/de/wschill) vom Forschungsbereich [Transformation der Energiewirtschaft](https://twitter.com/transenerecon) der Abteilung [Energie, Verkehr, Umwelt](https://www.diw.de/de/diw_01.c.604205.de/abteilung_energie__verkehr__umwelt.html) am [DIW Berlin](https://www.diw.de/de) im Kontext des vom BMBF geförderten Kopernikus-Projekts [Ariadne](https://ariadneprojekt.de/) erstellt. Ansprechpartner im Fall von Rückfragen ist [Wolf-Peter Schill](https://www.diw.de/cv/de/wschill).
