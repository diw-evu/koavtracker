---
hide:
  - navigation
#  - toc
---

# Ausbau erneuerbarer Energie

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Photovoltaik

Für die Photovoltaik hat sich die Koalition ein Ausbauziel auf 200 GW bis 2030 gesetzt. Im Monat September 2021 betrug die in Deutschland installierte Leistung nach Angaben der [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) knapp 58 GW. Zur Erreichung des Ziels müssen bis Ende 2030 somit im Durchschnitt also ca. 1,3 GW pro Monat zugebaut werden (blau gestrichelte Linie im Diagramm). Dieser Zubau muss netto erfolgen, also unter Berücksichtigung der im Zeitverlauf vom Netz gehenden Altanlagen. Zuletzt war der Ausbau deutlich geringer. Würde der PV-Zubau dem Trend der Jahre 2017-2021 weiter folgen, könnten bis Ende 2030 lediglich rund 90 GW erreicht werden (orange gestrichelte Linie). In den letzten zwölf Monaten war das Ausbautempo nur leicht höher, auch eine Fortführung dieses Trends würde das Ziel sehr deutlich verfehlen (rot gestrichelte Linie). Zur Erreichung des 200-GW-Ziels im Jahr 2030 muss der Ausbau also mehr als drei mal schneller erfolgen als im Durchschnitt der letzten zwölf Monate. 

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das PV-Ziel der Koalition für das Jahr 2030 liegt am oberen Ende des von den Ariadne-Szenarien aufgespannten Korridors (hier ohne das Modell TIMES). Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 sogar etwas über dem Ziel der der Koalition. Nach 2030 steigt die PV-Leistung in den Ariadne-Szenarien weiter an.

<iframe title = "Abbildung installierte Leistung Photovoltaik" src="../figures/pv_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Windkraft 

### an Land

Für die Windkraft an Land ist im Koalitionsvertrag kein explizites Zubauziel genannt. Aus dem genannten Strombedarf von 680-750 TWh und den Ausbauzielen der anderen erneuerbaren Energien ließ sich zunächst ein implizites Ausbauziel von ca. 100-120 GW bis 2030 ableiten (vgl. Abschätzung von [Hanns Koenig](https://twitter.com/HannsKoenig/status/1463554571607945217?s=20)). Laut der sogenannten [Eröffnungsbilanz Klimaschutz](https://www.bmwi.de/Redaktion/DE/Downloads/Energie/220111_eroeffnungsbilanz_klimaschutz.html) des BMWK peilt die Regierung nun ein Ausbauziel von 100 GW im Jahr 2030 an. Im Monat September 2021 betrug die in Deutschland installierte Leistung nach Angaben der [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) knapp 56 GW. Zur Zielerreichung müssen also bis Ende 2030 im Durchschnitt ca. 0,4 GW pro Monat (wiederum netto) zugebaut werden. 

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für den Ausbau der Windkraft an Land im Jahr 2030 liegt ungefähr in der Mitte des von allen Ariadne-Szenarien aufgespannten Korridors. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 rund 30 GW und über dem Ziel der Koalition. Danach steigt die installierte Windkraftleistung in den meisten Ariadne-Szenarien weiter deutlich an.

<iframe title = "Abbildung installierte Leistung Windkraft an Land" src="../figures/wind_onshore_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Für die Nutzung der Windenergie an Land sollen dem Koalitionsvertrag zufolge "zwei Prozent der Landesflächen ausgewiesen werden", wobei kein Zieljahr genannt wird. Bundesweit waren dem jüngsten [Bericht](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) des Bund-Länder-Kooperationsausschusses zufolge zum 31.12.2020 0,70 Prozent der Fläche rechtswirksam für Windenergie an Land ausgewiesen (unterer Korridor ohne Doppelzählungen, Flächenfestlegungen entweder ausschließlich auf Ebene der Raumordnung oder auf Bauleitplanebene). Um das Ziel zu erreichen, muss dieser Anteil somit mehr als verdoppelt werden.

<iframe title = "Abbildung ausgewiesene Landesflächen für Windkraft an Land" src="../figures/wind_areas_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### auf See

Für die Windkraft auf See strebt die Koalition eine Leistung von 30 GW im Jahr 2030 an. Im Monat September 2021 betrug die in deutschen Gewässern installierte Leistung nach Angaben der [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) bei 7,8 GW. Um das Ausbauziel zu erreichen, müssen bis 2030 im Durchschnitt ca. 0,2 GW pro Monat (netto) zugebaut werden. 

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für den Ausbau der Windkraft auf See im Jahr 2030 liegt über dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 nur gut halb so hoch wie das Ziel der der Koalition. Der Ausbau der Windenergie auf See nimmt in den Ariadne-Szenarien erst nach 2025 Fahrt auf. Bis 2045 steigt die auf See installierte Windkraftleistung in manchen Ariadne-Szenarien weiter stark an auf im Extremfall bis zu 80 GW im Jahr 2045.

<iframe title = "Abbildung installierte Leistung Windkraft auf See"src="../figures/wind_offshore_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Anteile im Stromsektor

Die Regierungskoalition strebt an, den Anteil erneuerbarer Energien am Bruttostrombedarf (bzw. Bruttostromverbrauch, BSV) bis zum Jahr 2030 auf 80 Prozent zu steigern. Statistische Daten zu diesem Indikator werden vom BMWK grundsätzlich jährlich bereit gestellt, z.B. [hier](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html) oder [hier](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html). Im Jahr 2020 betrug der Anteil 45,1 Prozent, im Jahr 2021 waren es nur noch 41,9 Prozent, wobei diese Angaben noch nicht endgültig sind (*). Dieser Rückgang dürfte weitgehend durch den im Jahr 2020 pandemiebedingt deutlich niedrigeren Stromverbrauch und durch ein relativ schlechtes Windjahr 2021 bedingt sein. Zur Erreichung des 2030-Ziels muss der Anteil ab 2021 im Durchschnitt um über vier Prozentpunkte pro Jahr wachsen.

Ergänzend können in der Abbildung auch die Anteile erneuerbarer Energien an der Nettostromerzeugung (NSE) dargestellt werden (durch Klick auf den entsprechenden Eintrag in der Legende). Sie werden im Rahmen der [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) des Fraunhofer ISE regelmäßig aktualisiert und liegen somit sehr viel früher vor als der im Koalitionsvertrag genannte Indikator. Die Abbildung zeigt die vorläufigen Daten, die zum Zeitpunkt der letzten Aktualisierung dieser Webseite verfügbar waren, d.h. im Lauf des Jahres kann sich der Anteil aufgrund saisonaler Effekte noch deutlich verändern (*). Die Anteile erneuerbarer Energien an der NSE und am BSV unterscheiden sich unter anderem deshalb, da der Nenner im ersten Fall aufgrund des Kraftwerkseigenverbrauchs geringer ist. 

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für den Anteil der erneuerbaren Energien im Jahr 2030 liegt in dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 mit 87 Prozent sogar über dem Ziel der Koalition von 80 Prozent. Nach 2030 steigt der Anteil in den meisten Ariadne-Szenarien auf über 90 Prozent an.

<iframe title = "Abbildung Anteil erneuerbare Energien Strom" src="../figures/resshares_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Wärmepumpen

Mit Wärmepumpen kann Umweltwärme nutzbar gemacht werden. Dies spielt in vielen Zukunftsszenarien insbesondere für den Raumwärmebereich eine große Rolle. Die Regierung hat in ihrem Koalitionsvertrag kein Ziel für den Ausbau von Wärmepumpen genannt - der Begriff "Wärmepumpe" wird im gesamten Dokument kein einziges mal verwendet. In der [Eröffnungsbilanz Klimaschutz](https://www.bmwi.de/Redaktion/DE/Downloads/Energie/220111_eroeffnungsbilanz_klimaschutz.html) des BMWK wird jedoch ein Korridor von "4,1 bis 6 Millionen Wärmepumpen" im Jahr 2030 genannt, wobei diese Zahl sich auf die Bereitstellung von Raumwärme in Einzelgebäuden beziehen dürfte (ohne Großwärmepumpen in Wärmenetzen und ohne Hochtemperaturwärmepumpen). Wir stellen in der folgenden Abbildung die Mitte dieses Korridors als Ampel-Ziel dar und zeigen zudem die historische Entwicklung des Bestands. Bis 2030 müssen somit jährlich rund 0,4 Millionen Wärmepumpen zugebaut werden. Als Datenquelle bis 2020 dient [EurObserv'ER](https://www.eurobserv-er.org/online-database/#), der Bestand des Jahres 2021 ist abgeschätzt (*) mit Absatzzahlen des [BWP](https://www.waermepumpe.de/presse/zahlen-daten/).

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Leitmodell für Wärmepumpen, REMod, ermittelt im *Technologiemix-Szenario* einen Bestand von 5,3 Million Wärmepumpen im Jahr 2030 (in Einzelgebäuden), was ungefähr dem Regierungsziel entspricht. Bis 2045 wächst dieser Wärmepumpen-Bestand auf rund 15 Millionen an. Außerdem können die Wärmepumpen-Ausbaupfade vier weiterer Studien verglichen werden, die gemeinsam mit den Ariadne-Szenarien als "Big 5"-Szenarien bezeichnet wurden, nämlich "Klimaneutrales Deutschland 2045" von [Agora Energiewende](https://www.agora-energiewende.de/veroeffentlichungen/klimaneutrales-deutschland-2045/), "Klimapfade 2.0" (Zielpfad) des [BDI](https://bdi.eu/artikel/news/klimapfade-2-0-deutschland-braucht-einen-klima-aufbruch/), "Leitstudie Aufbruch Klimaneutralität" (Szenario KN100) der [Dena](https://www.dena.de/newsroom/meldungen/dena-leitstudie-aufbruch-klimaneutralitaet/), sowie die "Langfristszenarien 3" (Szenario TN-Strom) des [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/). Bis 2045 ist der Wärmepumpen-Zubau in den BMWK-Langfristszenarien besonders hoch, in der Dena-Leitstudie ist er am niedrigsten.

<iframe title = "Abbildung Wärmepumpen" src="../figures/hp_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Anteile an der Wärmeerzeugung

Die Koalition strebt "einen sehr hohen Anteil Erneuerbarer Energien bei der Wärme" an. Als quantiatives Ziel für 2030 enthält der Koalitionsvertrag jedoch kein explizites Ziel für erneuerbare Energien, sondern die Formulierung, dass bis zum Jahr 2030 50 Prozent der Wärme "klimaneutral" erzeugt werden müssen. Da andere Optionen wie importierter klimaneutraler Wasserstoff oder CO<sub>2</sub>-Abscheidung im Wärmebereich bis 2030 jedoch eher unrealistisch erscheinen, interpretieren wir das genannte Ziel hier daher leicht abweichend vom Wortlaut als Erneuerbare-Energien-Ziel. Im Jahr 2020 betrug der Anteil der erneuerbaren Energien an der gesamten Wärmeerzeugung in Deutschland den Energiedaten des [BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html) zufolge 15,6 Prozent. Bis 2030 muss dieser Anteil demnach jährlich um über drei Prozentpunkte steigen und damit sehr viel stärker als in den letzten Jahren.

<iframe title = "Abbildung Anteil erneuerbare Energien Wärme" src="../figures/resshare_heat_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
