---
hide:
  - navigation
#  - toc
---

# Electric Mobility

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Road transport

### Battery electric passenger cars

The government coalition has set a target of at least 15 million fully electric passenger cars by 2030, which we equate here with purely battery electric vehicles. At the end of October 2021, there were around 0.55 million such passenger cars in Germany, according to the [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html). To achieve the target, an average of 0.13 million vehicles per month must be added by 2030. The linear progression shown here is purely illustrative. In reality, a pathway that follows a logistic curve is more likely.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be shown in the figure. The coalition's target of 15 million fully electric passenger cars in the year 2030 lies within the corridor provided by all Ariadne scenarios. The Ariadne lead model for mobility, VECTOR21, is only slightly below the coalition's target in the *Technology Mix Scenario*. By 2045, the fleet grows to nearly 42 million electric vehicles in the lead model. It should be noted that the Ariadne scenarios also include some plug-in hybrid electric vehicles, and not only fully electric ones as formulated in the coalition target.

<iframe title = "Figure stock of battery electric vehicles" src="../figures/bev_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

In addition, the following figure shows the shares of purely battery-electric cars and plug-in hybrids in new car registrations per month. Although the coalition has not formulated a specific target for this, this indicator illustrates the dynamics of what is happening better than the stock indicator shown above.

<iframe title = "Figure new electric car registrations" src="../figures/bev_adm_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Charging infrastructure

The traffic light coalition has set a target of one million publicly and non-discriminatorily accessible charging points by 2030, with a focus on fast charging infrastructure. As of 1 November 2021, according to [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html), nearly 53,000 charging points were in operation, of which around 45,000 were normal charging points and nearly 8000 were fast charging points. To achieve the target, an average of around 8,600 new charging points must go into operation every month until 2030.

<iframe title = "Figure charging infrastructure" src="../figures/cs_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following figure shows the ratio of battery-electric cars and publicly accessible charging points. The government has not formulated an explicit target for this. The fleet and charging infrastructure targets for 2030 imply a ratio of 15 battery electric cars per charging point, although the breakdown between fast and normal charging points is unclear. Recently, the number of electric vehicles sharing a publicly accessible fast charging point has increased significantly.

<iframe title = "Figure BEV per charging point" src="../figures/bev_per_cp_en.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>

## Rail transport

### Electrification of the network

The government coalition plans to electrify 75 percent of the German rail network by 2030. According to [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html), 61.7% of the federally owned rail network was electrified in 2020. To achieve the target, the share must increase by an average of almost 1.5 percentage points per year until 2030.

<iframe title = "Figure electrification of the rail network" src="../figures/electrification_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
