---
hide:
  - navigation
#  - toc
---

# Électromobilité

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Transport routier

### Voitures particulières électriques à batterie

La coalition gouvernementale a fixé un objectif d'au moins 15 millions de voitures particulières entièrement électriques d'ici 2030, que nous assimilons ici aux véhicules purement électriques à batterie (i.e. nous négligeons les véhicules électriques à hydrogène et les véhicules hybrides). Fin octobre 2021, on comptait environ 0,55 million de voitures particulières de ce type en Allemagne, selon l'[Office fédéral des véhicules à moteur](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html) (*Kraftfahrt-Bundesamt*). Pour atteindre l'objectif, il faut ajouter en moyenne 0,13 million de véhicules par mois d'ici 2030. La progression linéaire présentée ici est purement illustrative. En réalité, une diffusion qui suit une courbe logistique est plus probable.

À titre de comparaison, les [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/) peuvent également être représentés sur la figure. L'objectif de la coalition, à savoir 15 millions de voitures particulières entièrement électriques en 2030, se situe dans l'intervalle formé par tous les scénarios Ariadne. L'objectif de la coalition n'est que légèrement inférieur au résultat du scénario *Mix Technologique* du modèle de référence de mobilité d'Ariadne (VECTOR21). En 2045, le parc automobile atteint près de 42 millions de véhicules électriques dans le modèle de référence. Il convient de noter que les scénarios d'Ariadne comprennent également des véhicules électriques hybrides rechargeables, et pas seulement des véhicules entièrement électriques comme le prévoit l'objectif de la coalition.

<iframe title = "Figure stock de véhicules électriques à batterie" src="../figures/bev_fr.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

En complément, nous montrons dans la figure suivante la part des voitures purement électriques à batterie et des hybrides rechargeables dans les nouvelles immatriculations mensuelles de voitures. Bien que la coalition n'ait pas formulé d'objectif spécifique à ce sujet, cet indicateur illustre mieux la dynamique des événements que l'indicateur de stock présenté ci-dessus.

<iframe title = "Figure nouvelles immatriculations mensuelles de voitures" src="../figures/bev_adm_fr.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Infrastructure de recharge

La coalition en feu tricolore a fixé l'objectif d'un million de points de recharge publiquement accessibles à tout un chacun d'ici 2030, en mettant l'accent sur les infrastructures de recharge rapide. Au 1er novembre 2021, selon la [Agence fédérale du réseau](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html) (*Bundesnetzagentur*), environ 53 000 points de charge étaient en service, dont 45 000 points de charge normaux et environ 8 000 points de charge rapides. Pour atteindre l'objectif, environ 8 600 nouveaux points de charge doivent être mis en service en moyenne chaque mois jusqu'en 2030.

<iframe title = "Figure infrastructure de recharge" src="../figures/cs_fr.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

Le graphique suivant montre le rapport entre les voitures électriques à batterie et les points de recharge accessibles au public. Le gouvernement n'a pas formulé d'objectif explicite à ce sujet. Les objectifs en matière de parc et d'infrastructure de recharge pour 2030 donnent une valeur de 15 voitures électriques à batterie par point de recharge, la répartition entre points de recharge rapide et normale n'étant pas claire. Le nombre de véhicules électriques qui se partagent un point de charge rapide accessible au public a nettement augmenté.

<iframe title = "Figure BEV per point de recharge" src="../figures/bev_per_cp_fr.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>

## Transport ferroviaire

### Électrification du réseau

La coalition gouvernementale prévoit d'électrifier 75 % du réseau ferroviaire allemand d'ici 2030. Selon la [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html), 61,7 % du réseau ferroviaire appartenant au gouvernement fédéral était électrifié en 2020. Pour atteindre l'objectif, cette part doit augmenter en moyenne de près de 1,5 point de pourcentage par an jusqu'en 2030.

<iframe title = "Figure électrification du réseau ferroviaire" src="../figures/electrification_fr.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>
