---
hide:
  - navigation
#  - toc
---

# Elektromobilität

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Straßenverkehr

### Batterieelektrische Pkw

Die Regierungskoalition hat sich ein Ziel von "mindestens 15 Millionen vollelektrische(n) Pkw bis 2030" gesetzt, was wir hier mit rein batterieelektrischen Fahrzeugen gleichsetzen. Ende Oktober 2021 gab es laut [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html) rund 0,55 Millionen solcher Pkw in Deutschland. Zur Erreichung des Ziels müssen bis zum Jahr 2030 im Durchschnitt 0,13 Millionen Fahrzeuge pro Monat hinzu kommen. Der hier dargestellt lineare Verlauf ist dabei rein illustrativ. In Wirklichkeit ist eher mit einem S-kurvenförmigen Verlauf zu rechnen.

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für vollelektrische Pkw im Jahr 2030 liegt mitten in dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für Mobilität, VECTOR21, liegt im *Technologiemix-Szenario* nur knapp unter dem Ziel der Koalition. Bis zum Jahr 2045 wächst die Flotte im Leitmodell auf knapp 42 Millionen Fahrzeuge an. Zu beachten ist hier jedoch, dass die gezeigten Ariadne-Szenarien auch einen kleineren Anteil Plug-in-Hybridfahrzeuge beinhalten, also nicht nur vollelektrische Fahrzeuge wie beim formulierten Koalitionsziel.

<iframe title = "Abbildung Bestand batterieelektrische Pkws" src="../figures/bev_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Ergänzend zeigen wir in der folgenden Abbildung die Anteile von rein batterieelektrischen Pkw sowie von Plug-in-Hybriden an den monatlichen Neuzulassungen. Hierfür hat die Ampel-Koalition zwar kein spezifischen Ziel formuliert; dieser Indikator verdeutlicht die Dynamik des Geschehens jedoch besser als der oben gezeigte Bestandsindikator.

<iframe title = "Abbildung Neuzulassungen elektrische Pkws" src="../figures/bev_adm_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Ladeinfrastruktur

Die Ampel hat sich ein Ziel von "einer Million öffentlich und diskriminierungsfrei zugänglichen Ladepunkten bis 2030 mit Schwerpunkt auf Schnellladeinfrastruktur" gesetzt. Zum 1. November 2021 waren laut [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html) knapp 53.000 Ladepunkte in Betrieb, davon ca. 45.000 Normalladepunkte und knapp 8000 Schnellladepunkte. Zur Erreichung des Ziels müssen bis 2030 im Durchschnitt monatlich rund 8600 neue Ladepunkte in Betrieb gehen.

<iframe title = "Abbildung Ladeinfrastruktur" src="../figures/cs_de.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>

Die folgende Abbildung zeigt das Verhältnis von batterieelektrischen Pkw und öffentlich zugänglichen Ladepunkten. Hierfür hat die Regierung kein explizites Ziel formuliert. Aus den Bestands- und Ladeinfrastrukturzielen für 2030 ergibt sich ein Wert von 15 batterieelektrischen Pkw pro Ladepunkt, wobei die Aufteilung auf Schnell- und Normalladepunkte unklar ist. Zuletzt ist die vor allem die Zahl der Elektrofahrzeuge, die sich rechnerisch einen öffentlich zugänglichen Schnellladepunkt teilen, deutlich angestiegen.

<iframe title = "Abbildung BEV pro Ladepunkt" src="../figures/bev_per_cp_de.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>

## Schienenverkehr

### Elektrifizierung des Netzes

Die Ampelkoalition plant, bis zum Jahr 2030 75 Prozent des deutschen Schienennetzes zu elektrifizieren. Nach Angaben der [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html) waren im Jahr 2021 61,7% des bundeseigenen Schienennetzes elektrifiziert. Zur Erreichung des Ziel muss der Anteil bis 2030 im Durchschnitt um knapp 1,5 Prozentpunkte pro Jahr steigen.

<iframe title = "Abbildung Elektrifizierung des Schienennetzes" src="../figures/electrification_de.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>
