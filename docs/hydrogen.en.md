---
hide:
  - navigation
#  - toc
---

# Hydrogen

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

The coalition agreement mentions an electrolysis capacity of around 10 gigawatts in 2030 as a target. According to the [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database), electrolysers with an electrical capacity of 61 megawatts were in operation in Germany at the beginning of October 2021. According to this, an average of about 90 megawatts per month must be added by the end of 2030. As in the case of battery-electric vehicles, the linear progression shown here is purely illustrative; in reality, we rather expect the capacity expansion to follow a logistic curve.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figuree. The coalition's electrolysis target for 2030 is at the upper end of the corridor provided by all Ariadne scenarios. The Ariadne lead model for electrolysis capacity expansion, REMIND, is substantially below the coalition's target in the *Technology Mix Scenario* in 2030. After 2035, the Ariadne scenario corridor becomes very wide, with the lead model always at the lower end of the corridor. These model results reflect a relatively high uncertainty about the longer-term prospects of domestic green hydrogen production in Germany.

<iframe title = "Figure installed electrolysis capacity" "src="../figures/h2_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>