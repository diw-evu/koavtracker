---
hide:
  - navigation
#  - toc
---

# Sécurité d'approvisionnement 

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

Dans la sous-section sur le gaz et l'hydrogène, l'accord de coalition comprend une déclaration selon laquelle le gouvernement souhaite diversifier l'approvisionnement énergétique de l'Allemagne et de l'Europe. Pourtant, aucun objectif quantitatif n'est mentionné. Cependant, comme le rappelle l'attaque de la Russie contre l'Ukraine en février 2022, il est clair que la dépendance aux importations de gaz naturel, en particulier en provenance de Russie, doit être réduite rapidement. Dans le graphique suivant, nous montrons l'évolution des importations mensuelles de gaz naturel depuis 2010. Nous utilisons les données mensuelles sur les flux bruts aux différents points de passage des frontières fournies par l'[AIE](https://www.iea.org/data-and-statistics/data-product/gas-trade-flows#gas-trade-flows). Le graphique montre les pays regroupés par importations nettes et exportations nettes, ainsi que les importations nettes totales. Les valeurs nettes spécifiques à chaque pays ont été calculées en soustrayant les exportations (brutes) allemandes mensuelles vers un pays donné des importations (brutes) en provenance de ce même pays. Veuillez noter qu'il existe certaines divergences ainsi que des ambiguïtés de définition entre les différents types de statistiques sur le gaz naturel, voir également cette [discussion en ligne (en allemand)](https://twitter.com/WPSchill/status/1499125642880770053?s=20&t=Kzco4yO1MDm6HDfXiyLL0Q).

La dépendance à l'égard des importations de gaz naturel russe s'est considérablement accrue ces dernières années. Les importations directes (via Nord Stream 1) et indirectes (via la Pologne) de gaz en provenance de Russie sont du même ordre de grandeur que les importations nettes totales depuis 2018. Il convient de noter que l'Allemagne est également un pays de transit du gaz. En particulier, un part du gaz importé est régulièrement transférée vers la République tchèque depuis 2014.

<iframe title = "Figure importations de gaz allemand" src="../figures/gas_fr.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
