---
hide:
  - navigation
#  - toc
---

# Hydrogène

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

Le contrat de coalition mentionne comme objectif "une capacité d'électrolyse d'environ 10 gigawatts en 2030". Selon [la base de données de l'AIE sur les projets hydrogène](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database) (*IEA Hydrogen Projects Database*), début octobre 2021, des électrolyseurs d'une puissance électrique de 61 mégawatts étaient en service en Allemagne. Par conséquent, il faudra ajouter environ 90 mégawatts par mois en moyenne d'ici fin 2030. Comme dans le cas des véhicules électriques à batterie, l'évolution linéaire représentée ici est purement illustrative. En réalité, nous nous attendons plutôt à une diffusion en "S".

À titre de comparaison, les [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [Projet Ariadne](https://ariadneprojekt.de/) peuvent également être ajoutés au graphique. L'objectif de la coalition en matière d'électrolyse pour 2030 se situe dans la partie supérieure du couloir formé par l'ensemble des scénarios Ariadne. Dans le scénario *Mix Technologique* du modèle de référence d'Ariadne (REMIND), l'expansion de la capacité d'électrolyse en 2030 est sensiblement inférieur à l'objectif de la coalition. Après 2035, l'intervalle des scénarios Ariadne devient très large, le modèle de référence se situant toujours à l'extrémité inférieure de l'intervalle. Les résultats de ces modèles reflètent une incertitude relativement élevée quant aux perspectives à long terme de la production nationale d'hydrogène vert en Allemagne.

<iframe title = "Figure capacité d'électrolyse installée" src="../figures/h2_fr.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>