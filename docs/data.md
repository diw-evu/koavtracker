---
hide:
  - navigation
#  - toc
---

# Daten

## Ariadne-Szenariodaten
[Ariadne-Pathfinder](https://pathfinder.ariadneprojekt.de/) sowie [Ariadne Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne)

## Erneuerbare Energien

### Ausbau

Photovoltaik: [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) 

Windkraft an Land: [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat)

Windkraft zur See: [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat)

### Flächen

Windkraft an Land: [Bericht des Bund-Länder-Kooperationsausschusses](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) 

### Anteile im Stromsektor

Anteil am Bruttostrombedarf: [BMWK](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html)

Anteil an der Nettostromerzeugung: [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) 

### Wärmepumpen

Historische Entwicklung des Bestands: [EurObserv'ER](https://www.eurobserv-er.org/online-database/#) für 2011-2020, [BWP](https://www.waermepumpe.de/presse/zahlen-daten/) für 2021

Regierungsziel: [Eröffnungsbilanz Klimaschutz](https://www.bmwi.de/Redaktion/DE/Downloads/Energie/220111_eroeffnungsbilanz_klimaschutz.html)

Bestand in Zukunftsszenarien:

* "Klimaneutrales Deutschland 2045": [Agora Energiewende](https://www.agora-energiewende.de/veroeffentlichungen/klimaneutrales-deutschland-2045/)
* "Klimapfade 2.0": [BDI](https://bdi.eu/artikel/news/klimapfade-2-0-deutschland-braucht-einen-klima-aufbruch/)
* "Leitstudie Aufbruch Klimaneutralität": [Dena](https://www.dena.de/newsroom/meldungen/dena-leitstudie-aufbruch-klimaneutralitaet/)
* "Langfristszenarien 3": [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/)

### Anteile an der Wärmeerzeugung

[BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html)

## Elektromobilität

### Straßenverkehr

Batterieelektrische Pkw: [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html)

Ladeinfrastruktur: [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html), Spreadsheet "Ladeinfrastruktur in Zahlen"; Lineare Interpolation zwischen historischen Quartalsdaten.

### Schienenverkehr

Elektrifizierung des Netzes: [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html)

## Wasserstoff

Elektrolysekapazitäten: [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database)
