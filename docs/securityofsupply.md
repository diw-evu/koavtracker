---
hide:
  - navigation
#  - toc
---

# Versorgungssicherheit

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

Im Unterabschnitt zu Gas und Wasserstoff enthält der Koalitionsvertrag die Aussage: "Wir wollen die Energieversorgung für Deutschland und Europa diversifizieren."  Ein quantitatives Ziel wird dabei nicht genannt. Spätestens nach dem Angriff Russlands auf die Ukraine im Februar 2022 ist aber klar, dass insbesondere die Abhängigkeit von russischen Erdgasimporten schnell reduziert werden soll. In der folgenden Abbildung zeigen wir, wie sich die monatlichen Erdgasimporte seit dem Jahr 2010 entwickelt haben. Als Quelle dienen monatliche Daten der Brutto-Flüsse an einzelnen Grenzübergangspunkten der [IEA](https://www.iea.org/data-and-statistics/data-product/gas-trade-flows#gas-trade-flows). In der Abbildung sind die Länder nach Netto-Importen bzw. Netto-Exporten gruppiert, zudem sind die gesamten Netto-Importe angegeben. Die Nettowerte pro Land wurden errechnet, indem die monatlichen (Brutto-)Exporte in das jeweilige Land von den (Brutto-)Importen aus dem gleichen Land abgezogen wurden. In Hinblick auf auf die Datenlage bei Erdgasimporten gibt es übrigens einige Diskrepanzen sowie definitorische Unklarheiten zwischen verschiedenen Statistiken, vgl. auch diese Online-Diskussion [hier](https://twitter.com/WPSchill/status/1499125642880770053?s=20&t=Kzco4yO1MDm6HDfXiyLL0Q).

Die Abhängigkeit von russischen Erdgasimporten ist in den letzten Jahren deutlich gewachsen. Direkte (via Nord Stream 1) und indirekte (via Polen) Gasimporte aus Russland liegen seit 2018 in der gleichen Größenordnung wie die gesamten Netto-Importe. Zu beachten ist dabei, dass Deutschland auch ein ein Gas-Transitland ist. Insbesondere fließt seit 2014 regelmäßig Gas nach Tschechien.

<iframe title = "Abbildung deutsche Gasimporte" src="../figures/gas_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>