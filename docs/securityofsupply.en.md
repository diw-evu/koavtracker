---
hide:
  - navigation
#  - toc
---

# Security of supply

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

In the subsection on gas and hydrogen, the coalition agreement includes a statement that the government wants to diversify the energy supply for Germany and Europe. Yet, no quantitative target is mentioned. However, at the latest after Russia's attack on Ukraine in February 2022, it is clear that dependence on Russian natural gas imports in particular is to be reduced quickly. In the following graph, we show how monthly natural gas imports have developed since 2010. We use monthly data on gross flows at individual border crossing points provided by [IEA](https://www.iea.org/data-and-statistics/data-product/gas-trade-flows#gas-trade-flows). The graph shows countries grouped by net imports and net exports, as well as total net imports. The country-specific net values were calculated by subtracting monthly German (gross) exports to a given country from (gross) imports from the same country. Please note that there are some discrepancies as well as definitional ambiguities between different types of natural gas statistics, see also this [online discussion (in German)](https://twitter.com/WPSchill/status/1499125642880770053?s=20&t=Kzco4yO1MDm6HDfXiyLL0Q).

Dependence on Russian natural gas imports has grown significantly in recent years. Direct (via Nord Stream 1) and indirect (via Poland) gas imports from Russia have been in the same order of magnitude as total net imports since 2018. It should be noted that Germany is also a gas transit country. In particular, gas has been flowing regularly to the Czech Republic since 2014.

<iframe title = "Figure German gas imports" src="../figures/gas_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>