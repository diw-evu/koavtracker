---
hide:
  - navigation
#  - toc
---

# Données

## Scénarios Ariadne

[Ariadne-Pathfinder](https://pathfinder.ariadneprojekt.de/) et [Ariadne Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne)

## Énergies renouvelables

### Développement de capacités

Solaire photovoltaïque: [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) 

Éolien terrestre: [AGEE-Stat](https://umweltbundesamt.de/sites/default/files/medien/372/dokumente/11-2021_agee-stat_monatsbericht_final.pdf)

Éolien en mer: [AGEE-Stat](https://umweltbundesamt.de/sites/default/files/medien/372/dokumente/11-2021_agee-stat_monatsbericht_final.pdf)

### Superficies

Éolien terrestre: [Report of the Bund/Länder Cooperation Committee](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) 

### Part dans le secteur de l'électricité

Part de la consommation brute: [BMWi](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html)

Part de la production nette d'électricité: [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) 

### Pompes à chaleur 

Évolution historique du stock: [EurObserv'ER](https://www.eurobserv-er.org/online-database/#), [BWP](https://www.waermepumpe.de/presse/zahlen-daten/)

Objectif du gouvernement: [Bilan d'ouverture sur la protection du climat](https://www.bmwk.de/Redaktion/FR/Downloads/E/bilan-douverture-sur-la-protection-du-climat.html)

Stock dans les scénarios futurs: 

* "Towards a climate-neutral Germany by 2045" [Agora Energiewende](https://www.agora-energiewende.de/en/publications/towards-a-climate-neutral-germany-2045-executive-summary/)
* "Climate Paths 2.0" [BDI](https://english.bdi.eu/publication/news/climate-paths-2-0-a-program-for-climate-and-germanys-future-development/)
* "Pilot study: Towards Climate Neutrality" [Dena](https://www.dena.de/en/newsroom/news/dena-pilot-study-towards-climate-neutrality)
* "Langfristszenarien 3" [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/)

### Part dans la production de chaleur

[BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html)

## Électromobilité

### Transport routier

Voitures particulières électriques à batterie: [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html)

Points de recharge: [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html), feuille de calcul "Ladeinfrastruktur in Zahlen"; Interpolation linéaire entre les données trimestrielles historiques.

### Transport ferroviaire

Électrification du réseau: [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html)

## Hydrogène

Capacité d'électrolyse: [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database)
