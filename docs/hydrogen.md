---
hide:
  - navigation
#  - toc
---

# Wasserstoff

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

Im Koalitionsvertrag wird "eine Elektrolysekapazität von rund 10 Gigawatt im Jahr 2030" als Ziel genannt. Laut [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database) waren Anfang Oktober 2021 in Deutschland Elektrolyseure mit einer elektrischen Leistung von 61 Megawatt in Betrieb. Demnach müssen bis Ende 2030 im Durchschnitt etwa 90 Megawatt pro Monat zugebaut werden. Wie bereits im Fall der batterielektrischen Fahrzeuge ist der hier dargestellte lineare Verlauf rein illustrativ. In Wirklichkeit erwarten wir eher einen S-Kurven-förmigen Ausbaupfad.

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Elektrolyse-Ziel der Koalition für das Jahr 2030 liegt am oberen Ende des von allen Ariadne-Szenarien aufgespannten Korridors. Das Ariadne-Leitmodell für den Elektrolyse-Zubau, REMIND, liegt im *Technologiemix-Szenario* 2030 deutlich unter dem Ziel der Koalition. Nach 2035 spannen die Ariadne-Szenarien einen sehr breiten Korridor auf, wobei das Leitmodell dauerhaft im unteren Bereich des Korridors liegt. Grundsätzlich spiegeln die Modellergebnisse eine relativ große Unsicherheit über die längerfristige heimische Produktion von grünem Wasserstoff wider.

<iframe title = "Abbildung installierte Elektrolysekapazität" src="../figures/h2_de.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
