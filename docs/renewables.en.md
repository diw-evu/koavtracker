---
hide:
  - navigation
#  - toc
---

# Renewable energy expansion

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Solar PV

For solar PV, the government coalition has set a capacity expansion target of 200 GW by 2030. According to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat), the installed capacity in Germany in September 2021 was just under 58 GW. To achieve the target, an average of approx. 1.3 GW per month must be added by the end of 2030 (blue dotted line in the diagram). This is the required net capacity deployment, i.e. also taking into account old plants that will be taken off the grid over time. Over the last years, the expansion was significantly lower than that. If PV capacity additions were to follow the trend of the years 2017-2021, only around 90 GW could be reached by the end of 2030 (orange dashed line). In the last twelve months, the pace of expansion was only slightly higher, so a continuation of this trend would also fall significantly short of the target (red dashed line). To reach the 200 GW target in 2030, the expansion must therefore be more than three times faster than the average of the last twelve months.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be shown in the figure. The PV target of the coaltion for the year 2030 is at the upper end of the corridor of all Ariadne scenarios (here without the model TIMES). The Ariadne lead model for the expansion of renewable energy sources, REMIND, is even slightly above the coaltion target in the *Technology Mix Scenario* for 2030. After 2030, PV capacity increases further in the Ariadne scenarios.

<iframe title = "Figure installed pv solar capacity" src="../figures/pv_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Wind energy 

### Onshore

For onshore wind power, the coalition agreement does not explicitly mention a capacity expansion target. From the stated electricity demand of 680-750 TWh and the expansion targets for other renewable energy sources, an implicit expansion target of approx. 100-120 GW by 2030 could initially be derived (cf. estimate by [Hanns Koenig](https://twitter.com/HannsKoenig/status/1463554571607945217?s=20)). According to the so-called [*Eröffnungsbilanz Klimaschutz*](https://www.bmwi.de/Redaktion/DE/Downloads/Energie/220111_eroeffnungsbilanz_klimaschutz.html) of the federal ministry in charge (BMWK), the government is now aiming for an expansion target of 100 GW in 2030. According to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) data, the installed capacity in Germany in September 2021 was just under 56 GW. To achieve the target, an average of approx. 0.4 GW per month (again net) must be added by the end of 2030.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's target for the expansion of onshore wind power in 2030 is roughly in the middle of the corridor formed by all Ariadne scenarios. The Ariadne lead model for renewable energy expansion, REMIND, is around 30 GW above the coalition's target in the *Technology Mix Scenario* in 2030. Afterwards, wind power capacity increases further in most Ariadne scenarios.

<iframe title = "Figure onshore wind capacity" src="../figures/wind_onshore_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

According to the coalition agreement, two percent of the land is to be designated for the use of onshore wind energy, although no target year is mentioned. According to the most recent [report](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) of the *Bund-Länder Kooperationsausschuss*, 0.70 per cent of the land was legally designated for onshore wind energy as of 31 December 2020 (lower corridor without double counting, land designated either exclusively at the level of spatial planning or at the level of urban land-use plans). To achieve the target, this proportion must therefore be more than doubled.

<iframe title = "Figure area designated to wind onshore", src="../figures/wind_areas_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Offshore

For offshore wind power, the government coalition aims at a capacity of 30 GW in 2030. In the month of September 2021, the installed capacity in German waters was 7.8 GW, according to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat). In order to achieve the expansion target, an average of approx. 0.2 GW per month (net) must be added to meet the 2030 target.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's target for the expansion of offshore wind power in 2030 is above the Ariadne scenario corridor. In the Ariadne lead model for the expansion of renewable energy sources, REMIND, the installed capacity is only about half as high as the coalition's target in the *Technology Mix Scenario* in 2030. The expansion of offshore wind energy in the Ariadne scenarios only picks up speed after 2025. By 2045, the installed capacity increases further in several Ariadne scenarios, up to 80 GW in the most extreme case.

<iframe title = "Figure installed offshore wind capacity" src="../figures/wind_offshore_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Shares in the power sector

The coalition aims to increase the share of renewable energy in gross electricity consumption to 80 percent by 2030. Statistical data on this indicator are provided by the federal ministry in charge, the BMWK on an annual basis, e.g., [here](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html) or [here](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html). In 2020, the share was 45.1 percent; in 2021, it was only 41.9 percent, although the data are not yet final (*). This decline is likely to be largely caused by significantly lower electricity consumption in 2020 due to the pandemic and a relatively poor wind year in 2021. To reach the 2030 target, the share must grow by more than four percentage points per year on average from 2021 onwards.

In addition, the shares of renewable energy in net electricity generation can also be shown in the figure (by clicking on the corresponding entry in the legend). These shares are regularly updated by Fraunhofer ISE's [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) and are thus available much earlier than the consumption-based indicator mentioned in the coalition agreement. The graph shows the data available at the time of the last update of this website, i.e. the share can still change significantly over the course of the current year due to seasonal effects (*). The shares of renewable energy in net electricity generation and gross electricity consumption differ, among other reasons, because the denominator is lower in the first case due to own consumption of power plants. 

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's target for the share of renewable energies in 2030 lies within the Ariadne scenario corridor. The Ariadne lead model for renewable energy expansion, REMIND, even exceeds the coalition's target of 80 percent in the *Technology Mix Scenario* in 2030 with a share of 87 percent. After 2030, the share increases beyond 90 percent in most Ariadne scenarios.

<iframe title = "Figure share of renewables in electricity" src="../figures/resshares_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Heat pumps

Heat pumps can be used to harness environmental heat. They play a major role in many future scenarios, especially for space heating. The German government did not mention a target for the expansion of heat pumps in its coalition agreement - the term "heat pump" does not appear a single time in the entire document. However, the [*Eröffnungsbilanz Klimaschutz*](https://www.bmwk.de/Redaktion/EN/Downloads/E/germany-s-current-climate-action-status.html) of the Federal Ministry for Economic Affairs and Climate Action (BMWK) mentions a corridor of "4.1 to 6 million heat pumps" in 2030. This figure is likely to refer to the provision of space heating in individual buildings (excluding large-scale heat pumps in heating networks and high-temperature heat pumps). In the following figure, we define the middle point of this interval as the coalition target and also show the historical development of the Geman heat pump stock. This means around 0.4 Million heat pumps per year have to be added until 2030. The data source until 2020 is [EurObserv'ER](https://www.eurobserv-er.org/online-database/#), the stock of 2021 is estimated (*) with sales figures from [BWP](https://www.waermepumpe.de/presse/zahlen-daten/).

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The lead model for heat pumps (REMod) identifies a stock of 5.3 million heat pumps in 2030 (in individual buildings) in the *Technology mix scenario*, which is roughly in line with the government target. By 2045, this heat pump inventory grows to about 15 million. In addition, the heat pump expansion paths of four other studies can be compared, which together with the Ariadne scenarios have been dubbed the "Big 5" scenarios, namely "Climate Neutral Germany 2045" by [Agora Energiewende](https://www.agora-energiewende.de/en/publications/towards-a-climate-neutral-germany-2045-executive-summary/), "Climate Paths 2.0" (scenario Zielpfad) by [BDI](https://english.bdi.eu/publication/news/climate-paths-2-0-a-program-for-climate-and-germanys-future-development/), "Towards climate neutrality" (*Leitstudie Aufbruch Klimaneutralität*, scenario KN100) by [Dena](https://www.dena.de/en/newsroom/news/dena-pilot-study-towards-climate-neutrality/), and "Long-term scenarios 3" (scenario TN-Strom) by [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/). Up to 2045, heat pump additions are particularly high in the BMWK long-term scenarios, and lowest in the Dena lead study.

<iframe title = "Abbildung Wärmepumpen" src="../figures/hp_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Shares in heat supply

The coalition aims for a "very high" share of renewables in the heating sector. Yet, a quantitative target is only provided for "climate neutral" heat in the coalition agreement, which should reach 50 percent of the total heat supply by 2030. As other options, such as imported climate-neutral hydrogen or carbon capture and storage appear to be implausible in the German heat sector by 2030, we interpret the quantiative target in fact as a renewable energy target, slightly deviating from the exact wording of the coalition agreement. In 2020, the renewable share in total German heat supply was 15.6 percent according to official data provided by [BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html). By 2030, this share has to increase by more than three percentage points per year, which is much more than what has been achieved in recent years.

<iframe title = "Figure share of renewables in heat" src="../figures/resshare_heat_en.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
