---
description: Le KoaVTracker visualise les objectifs et les avancées de la nouvelle coalition gouvernementale en Allemagne dans certains domaines de la transition énergétique.

hide:
  - navigation
  - toc
---

# #KoaVTracker

!!! danger "Nouveautés de la version 0.5.3"
    
    Le KoaVTracker contient désormais aussi des données des pompes à chaleur, des immatriculations mensuelles de véhicules électriques, le rapport entre les voitures électriques et les points de charge, et des données comparatives issues de l'[analyse de scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/).

## Objectifs énergétiques dans l'accord de coalition du gouvernement allemand par rapport à la situation actuelle

Dans leur [accord de coalition](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) du 24 novembre 2021, les partis de la coalition dite "en feu tricolore" se sont fixé divers objectifs quantitatifs pour le secteur énergétique allemand. Sur cette page web, nous mettons ces objectifs - pour la plupart spécifiés pour l'année 2030 - en graphique et nous les comparons à la situation actuelle. Nous nous concentrons d'abord sur les objectifs relatifs à l'expansion de la capacité des énergies renouvelables, à la mobilité électrique et à l'hydrogène. Pour certains de ces indicateurs, les données sont disponibles et mises à jour sur une base mensuelle ; pour d'autres, seules des données annuelles sont disponibles. Pour des raisons purement illustratives, une trajectoire linéaire est indiquée pour chaque indicateur entre la situation actuelle et l'année cible (généralement décembre 2030). Les trajectoires exactes n'ont pas été précisées dans l'accord de coalition. Pour certains indicateurs, nous incluons également des projections calculées à partir des tendances récentes. 

Les objectifs de la coalition et les résultats obtenus jusqu'à présent peuvent également être comparés, pour certains indicateurs, aux résultats des [scénarios Ariadne](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/). Il s'agit d'analyses de scénarios réalisées dans le cadre du projet Copernicus [Ariadne](https://ariadneprojekt.de/) financé par le ministère fédéral allemand de l'Enseignement et de la Recherche (BMBF). Ces analyses montrent différentes trajectoires vers la neutralité climatique pour l'Allemagne à horizon 2045. Nous présentons de surcroît le corridor défini par tous les scénarios Ariadne ainsi que les résultats du modèle de référence d'Ariadne correspondant au scénario *Mix Technologique* (en allemand: *Technologiemix-Szenario* 8Gt_Bal). Pour les scénarios Ariadne, les données ne sont en principe disponibles que par incrément de cinq ans (2020, 2025, 2030, ...). Nous relions les années de référence en supposant une trajectoire linéaire. Les données des scénarios Ariane sont disponibles jusqu'en 2045, ce qui permet d'avoir une vision à plus long terme des évolutions plausibles des différents indicateurs. D'autres visualisations ainsi que des données sur tous les scénarios Ariadne sont disponibles sur des sites web externes tels que [Pathfinder](https://pathfinder.ariadneprojekt.de/) et [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne). À l'avenir, de nouveaux indicateurs et des trajectoires supplémentaires sont susceptibles d'être mis en ligne.

Nous appelons cet outil "KoaVTracker". "KoaV" est l'abréviation de *Koalitionsvertrag*, qui signifie accord de coalition ; et "Tracker" - oui, nous aimons les anglicismes !

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Qui sommes-nous ?

Cette page a été créée par [Adeline Guéret](https://www.diw.de/cv/en/agueret), [Alexander Roth](https://www.diw.de/cv/en/aroth) et [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), chercheur.e.s du groupe de recherche [Transformation de l'économie de l'énergie](https://twitter.com/transenerecon) au sein du département [Énergie, Transport, Environnement](https://www.diw.de/en/diw_01.c.604205.en/energy__transportation__environment_department.html) du [DIW Berlin](https://www.diw.de/en). Elle s'inscrit dans le cadre du projet Copernicus [Ariadne](https://ariadneprojekt.de/) financé par le ministère fédéral allemand de l'Enseignement et de la Recherche (BMBF). Pour tout renseignement, veuillez contacter [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill).
