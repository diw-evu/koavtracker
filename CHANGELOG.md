# Änderungsprotokoll
*(for the moment only available in German)*

## 0.5.4 (2022-05-23)

Fix:

- Englische und französische Version der Abbildung "Elektrifizierter Anteil des Schienennetzes" werden jetzt wieder korrekt angezeigt

## 0.5.3 (2022-05-06)

Neu:

- Verhältnis von Elektro-Pkw zu Ladepunkten

## 0.5.2 (2022-05-04)

Neu:

- Anteil Elektro-Pkw and Hybrid-Pkw an Neuzulassungen

## 0.5.1 (2022-04-27)

Neu:

- "Big 5"-Szenarien der Wärmepumpenabbildung hinzugefügt

## 0.5.0 (2022-04-14)

Neu:

- Szenarien des Ariadneprojekts in verschiedene Abbildungen integriert
- Trends für batterieelektrische Pkw und Ladestationen
- Design der Abbildungen leicht angepasst
- Neue Daten

## 0.4.0 (2022-03-09)

Neu:

- Französische Version des KoaVTrackers

## 0.3.1 (2022-03-04)

Neu:

- Rubrik zur Versorgungssicherheit 
- Abbildung zu Erdgas-Nettoimporten

## 0.3.0 (2022-02-15)

Neu:

- Englische Version des KoaVTrackers

## 0.2.2 (2022-02-08)

Update:

- Designupdate für manche Abbildungen

## 0.2.1 (2022-02-07)

Neu:

- Historische Daten und Trendlinien in einigen Abbildungen

Update:

- Daten 

## 0.2.0 (2021-12-23)

Neu:

- Abbildungen in Unterseiten verteilt
- Neue Unterseite "Daten" 
- Neue Unterseite "Änderungen" (changelog)

Update:

- Legislaturperiode sichtbar in Abbildungen
- Farbschema der Abbildungen vereinheitlicht

## 0.1.4 (2021-12-20)

Update:

- Daten Dezember 2021

## 0.1.3 (2021-12-06)

Neu:

- Abbildungen laden schneller
- Anteile Erneuerbare Energien
- Flächen Windkraft
- Elektrifizierung Schienennetz

Update:

- Daten November 2021

## 0.1.2 (2021-12-03)

Neu:

- Kapazitäten von Elektrolyseuren

## 0.1.1 (2021-12-03)

Neu:

- Ladeinfrastruktur für Elektroautos

## 0.1.0 (2021-12-01)

Erste Veröffentlichung des KoaVTrackers.
