#%% Import packages

from pathlib import Path
from sklearn.linear_model import LinearRegression

import plotly.graph_objs as go

import numpy as np
import pandas as pd
from scipy import signal

#%% Create directory

Path("docs/figures").mkdir(parents=True, exist_ok=True)

#%% Create RES figure

def create_res_fig(data,title,legislative_period,reached,trend_2017_2021,trend_12_months,
                  linear_trend,target_coalition,only_legislative_period,til_2030,til_2050,
                  legend_title_trends,
                  legend_title_ariadne,ariadne_lead,ariadne_lead_points,ariadne_span,
                  y_axis_title,
                  y_max_2025,y_max_2030,y_max_2050):
    """
    Create main RES figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09", fillcolor = "green", opacity = 0.1,
                     layer = 'below', annotation_text = legislative_period,
                     annotation_position = "top left")
    fig.add_trace(go.Bar(x = data['date'], y = data['actual'], name = reached,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc",
                         legendgrouptitle_text=" "))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
                            name = trend_12_months, line = dict(color="#d62728", dash='dot'),
                            hovertemplate = '%{y:.2f}GW', visible='legendonly',
                            legendgroup="trends"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                            name = linear_trend, line = dict(color="#1f77b4", dash='dash'),
                            hovertemplate = '%{y:.2f}GW',
                            legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = target_coalition,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "#2ca02c", 
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead, 
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne"))

    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             ))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=only_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-07", "2025-12"],
                            'yaxis.range': [0, y_max_2025]}]),
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2017-01", "2031-03"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2017-01", "2046-06"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2017-01", "2031-03"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=120, b=0, pad=0),
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'h',
                  'x': 0.0, 'y': -0.4,
                  'yanchor': 'bottom', 'xanchor': 'left'})

    return fig

#%% pv #############################################################################################

pv_data               = pd.read_csv("docs/data/pv.csv", sep = ",")
pv_data['months']     = pv_data['months'].astype(str)
pv_data['date']       = pd.to_datetime(pv_data['date'], format='%d.%m.%Y')#.dt.strftime('%b %y')

# Create index
pv_data['X']          = pv_data.index

#%% trend 2017-2021
pv_reg1_data = pv_data[['date','X','actual']].dropna()
pv_reg1_data = pv_reg1_data[(pv_reg1_data['date'] > '2016-12') &
                            (pv_reg1_data['date'] <= '2021-09')]
pv_reg1_x    = np.array(pv_reg1_data['X']).reshape((-1, 1))
pv_reg1      = LinearRegression().fit(pv_reg1_x, pv_reg1_data['actual'])

predict1_pv = pv_data[['date','X','actual']]
predict1_pv = predict1_pv[(predict1_pv['date'] >= '2021-10')].reset_index()
predict1_pv['index'] = predict1_pv.index
predict1_pv['trend_since_2017'] = predict1_pv.loc[0,'actual'] + predict1_pv['index'] * pv_reg1.coef_
predict1_pv = predict1_pv[['date','trend_since_2017']]

#%% dynamic trend last 12 months
pv_reg2_data = pv_data[['date','actual','X']].dropna()
pv_reg2_data = pv_reg2_data.tail(12)
pv_reg2_x    = np.array(pv_reg2_data['X']).reshape((-1, 1))
pv_reg2      = LinearRegression().fit(pv_reg2_x, pv_reg2_data['actual'])

predict2_pv = pv_data[['date','X','actual']]
predict2_pv = predict2_pv[(predict2_pv['date'] >= pv_reg2_data.iloc[-1]['date'])].reset_index()
predict2_pv['index'] = predict2_pv.index
predict2_pv['trend_last_12months'] = predict2_pv.loc[0,'actual'] + predict2_pv[
    'index'] * pv_reg2.coef_
predict2_pv = predict2_pv[['date','trend_last_12months']]

#%% merge
pv_data = pv_data.merge(predict1_pv, how='left', on='date')
pv_data = pv_data.merge(predict2_pv, how='left', on='date')

#%% create pv figure

fig_pv_de = create_res_fig(pv_data,"Installierte Leistung Photovoltaik","Legislaturperiode",
                          "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
                          "Linearer Verlauf", "Ziel der Koalition",
                          "Nur Legislaturperiode", "bis 2030","bis 2045",
                          "Historische Trends",
                          "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                          "Szenariokorridor","Gigawatt",
                          140,250,550)

fig_pv_en = create_res_fig(pv_data,"Installed PV capacity","Legislative period",
                          "Actually reached","2017-2021","Last 12 months",
                          "Linear development","Target of the coalition",
                          "Legislative period only","until 2030","until 2045",
                          "Historic trends",
                          "Ariadne scenarios","Lead model","Lead model (interpolated)",
                          "Scenario corridor","Gigawatt",
                          140,250,550)

fig_pv_fr = create_res_fig(pv_data,"Capacité installée - photovoltaïque","Période du mandat",
                          "Déjà installée","2017-2021","12 derniers mois",
                          "Progression linéaire","Objectif de la coalition",
                          "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
                          "Tendances historiques",
                          "Scénarios Ariadne","Modèle de référence","Modèle de référence (tendance)",
                          "Intervalle de scénarios","Gigawatt",
                          140,250,550)

fig_pv_de.write_html("docs/figures/pv_de.html", include_plotlyjs="cdn")
fig_pv_en.write_html("docs/figures/pv_en.html", include_plotlyjs="cdn")
fig_pv_fr.write_html("docs/figures/pv_fr.html", include_plotlyjs="cdn")

#%% wind onshore ###################################################################################

windon_data           = pd.read_csv("docs/data/wind_onshore.csv", sep = ",")
windon_data['months'] = windon_data['months'].astype(str)
windon_data['date']   = pd.to_datetime(windon_data['date'], format='%d.%m.%Y')

# Create index
windon_data['X']      = windon_data.index

#%% trend 2017-2021
windon_reg1_data = windon_data[['date','X','actual']].dropna()
windon_reg1_data = windon_reg1_data[(windon_reg1_data['date'] > '2016-12') &
                                    (windon_reg1_data['date'] <= '2021-09')]
windon_reg1_x    = np.array(windon_reg1_data['X']).reshape((-1, 1))
windon_reg1      = LinearRegression().fit(windon_reg1_x, windon_reg1_data['actual'])

predict1_windon = windon_data[['date','X','actual']]
predict1_windon = predict1_windon[(predict1_windon['date'] >= '2021-10')].reset_index()
predict1_windon['index'] = predict1_windon.index
predict1_windon['trend_since_2017'] = predict1_windon.loc[0,'actual'] + predict1_windon[
    'index'] * windon_reg1.coef_
predict1_windon = predict1_windon[['date','trend_since_2017']]

#%% dynamic trend last 12 months
windon_reg2_data = windon_data[['date','actual','X']].dropna()
windon_reg2_data = windon_reg2_data.tail(12)
windon_reg2_x    = np.array(windon_reg2_data['X']).reshape((-1, 1))
windon_reg2      = LinearRegression().fit(windon_reg2_x, windon_reg2_data['actual'])

predict2_windon = windon_data[['date','X','actual']]
predict2_windon = predict2_windon[(predict2_windon['date'] >=
                                   windon_reg2_data.iloc[-1]['date'])].reset_index()
predict2_windon['index'] = predict2_windon.index
predict2_windon['trend_last_12months'] = predict2_windon.loc[0,'actual'] + predict2_windon[
    'index'] * windon_reg2.coef_
predict2_windon = predict2_windon[['date','trend_last_12months']]

#%% merge
windon_data = windon_data.merge(predict1_windon, how='left', on='date')
windon_data = windon_data.merge(predict2_windon, how='left', on='date')

#%% create windon figure

fig_windon_de = create_res_fig(windon_data,"Installierte Leistung Windkraft an Land",
                               "Legislaturperiode",
                               "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
                               "Linearer Verlauf", "Ziel der Koalition",
                               "Nur Legislaturperiode", "bis 2030","bis 2045",
                               "Historische Trends",
                               "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                               "Szenariokorridor","Gigawatt",
                               100,150,250)

fig_windon_en = create_res_fig(windon_data,"Installed capacity onshore wind power",
                               "Legislative period",
                               "Actually reached","2017-2021","Last 12 months",
                               "Linear development","Target of the coalition",
                               "Legislative period only","until 2030","until 2045",
                               "Historic trends",
                               "Ariadne scenarios","Lead model","Lead model (interpolated)",
                               "Scenario corridor","Gigawatt",
                               100,150,250)

fig_windon_fr = create_res_fig(windon_data,"Capacité installée - éolien terrestre",
                               "Période du mandat",
                               "Déjà installée","2017-2021","12 derniers mois",
                               "Progression linéaire","Objectif de la coalition",
                               "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
                               "Tendances historiques",
                               "Scénarios Ariadne","Modèle de référence","Modèle de référence (tendance)",
                               "Intervalle de scénarios","Gigawatt",
                               100,150,250)

fig_windon_de.write_html("docs/figures/wind_onshore_de.html", include_plotlyjs="cdn")
fig_windon_en.write_html("docs/figures/wind_onshore_en.html", include_plotlyjs="cdn")
fig_windon_fr.write_html("docs/figures/wind_onshore_fr.html", include_plotlyjs="cdn")

#%% wind offshore ##################################################################################

windoff_data           = pd.read_csv("docs/data/wind_offshore.csv", sep = ",")
windoff_data['months'] = windoff_data['months'].astype(str)
windoff_data['date']   = pd.to_datetime(windoff_data['date'], format='%d.%m.%Y')

# Create index
windoff_data['X']      = windoff_data.index

#%% trend 2017-2021
windoff_reg1_data = windoff_data[['date','X','actual']].dropna()
windoff_reg1_data = windoff_reg1_data[(windoff_reg1_data['date'] > '2016-12') &
                                      (windoff_reg1_data['date'] <= '2021-09')]
windoff_reg1_x    = np.array(windoff_reg1_data['X']).reshape((-1, 1))
windoff_reg1      = LinearRegression().fit(windoff_reg1_x, windoff_reg1_data['actual'])

predict1_windoff = windoff_data[['date','X','actual']]
predict1_windoff = predict1_windoff[(predict1_windoff['date'] >= '2021-10')].reset_index()
predict1_windoff['index'] = predict1_windoff.index
predict1_windoff['trend_since_2017'] = predict1_windoff.loc[0,'actual'] + predict1_windoff[
    'index'] * windoff_reg1.coef_
predict1_windoff = predict1_windoff[['date','trend_since_2017']]

#%% dynamic trend last 12 months
windoff_reg2_data = windoff_data[['date','actual','X']].dropna()
windoff_reg2_data = windoff_reg2_data.tail(12)
windoff_reg2_x    = np.array(windoff_reg2_data['X']).reshape((-1, 1))
windoff_reg2      = LinearRegression().fit(windoff_reg2_x, windoff_reg2_data['actual'])

predict2_windoff = windoff_data[['date','X','actual']]
predict2_windoff = predict2_windoff[(predict2_windoff['date'] >=
                                     windoff_reg2_data.iloc[-1]['date'])].reset_index()
predict2_windoff['index'] = predict2_windoff.index
predict2_windoff['trend_last_12months'] = predict2_windoff.loc[0,'actual'] + predict2_windoff[
    'index'] * windoff_reg2.coef_
predict2_windoff = predict2_windoff[['date','trend_last_12months']]

#%% merge
windoff_data = windoff_data.merge(predict1_windoff, how='left', on='date')
windoff_data = windoff_data.merge(predict2_windoff, how='left', on='date')

#%% create windoff figure

fig_windoff_de = create_res_fig(windoff_data,"Installierte Leistung Windkraft auf See",
                               "Legislaturperiode",
                               "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
                               "Linearer Verlauf", "Ziel der Koalition",
                               "Nur Legislaturperiode", "bis 2030","bis 2045",
                               "Historische Trends",
                               "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                               "Szenariokorridor","Gigawatt",
                               20,40,100)

fig_windoff_en = create_res_fig(windoff_data,"Installed capacity offshore wind power",
                               "Legislative period",
                               "Actually reached","2017-2021","Last 12 months",
                               "Linear development","Target of the coalition",
                               "Legislative period only","until 2030","until 2045",
                               "Historic trends",
                               "Ariadne scenarios","Lead model","Lead model (interpolated)",
                               "Scenario corridor","Gigawatt",
                               20,40,100)

fig_windoff_fr = create_res_fig(windoff_data,"Capacité installée éolien en mer",
                               "Période du mandat",
                               "Déjà installée","2017-2021","12 derniers mois",
                               "Progression linéaire","Objectif de la coalition",
                               "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
                               "Tendances historiques",
                               "Scénarios Ariadne","Modèle de référence","Modèle de référence (tendance)",
                               "Intervalle de scénarios","Gigawatt",
                               20,40,100)

fig_windoff_de.write_html("docs/figures/wind_offshore_de.html", include_plotlyjs="cdn")
fig_windoff_en.write_html("docs/figures/wind_offshore_en.html", include_plotlyjs="cdn")
fig_windoff_fr.write_html("docs/figures/wind_offshore_fr.html", include_plotlyjs="cdn")

#%% wind onshore areas data ########################################################################

wind_areas_data          = pd.read_csv("docs/data/wind_areas.csv", sep = ",")
wind_areas_data['date']  = pd.to_datetime(wind_areas_data['date'], format='%Y')

#%% create wind onshore areas figure

def create_fig_wind_areas(data,title,reached,target_coalition,percentage):
    """
    Create wind areas figure
    """

    fig_wind_areas = go.Figure()

    fig_wind_areas.add_trace(go.Bar(x = data['date'], y = wind_areas_data['actual'],
                                    name = reached, marker_color='#ff7f0e',
                                    hovertemplate = '%{y:.2f}%'))
    fig_wind_areas.add_trace(go.Scatter(x = wind_areas_data['date'], y = wind_areas_data['linear'],
                                name = target_coalition, mode = 'lines',
                                line = dict(color="#2ca02c", dash='dashdot'),
                                hovertemplate = '%{y:.2f}%'))
    fig_wind_areas.update_yaxes(matches = None, title = percentage)
    fig_wind_areas.update_xaxes(title=None,
                            tickmode = 'array',
                            tickvals = [2020, 2021, 2022, 2023, 2024],
                            ticktext = ['2020', '2021', '2022', '2023', '2024'])

    fig_wind_areas.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide', legend_title_text = '',
        legend = dict(yanchor="bottom", xanchor="left", x=0, y=-.3, orientation = "h"),
        margin=dict(l=0, r=0, t=130, b=0, pad=0),
        xaxis_hoverformat='%Y',
        xaxis_tickformat='%Y',
        hovermode = "x"
        )

    return fig_wind_areas

fig_wind_areas_de = create_fig_wind_areas(wind_areas_data,
                      "Anteil der für Windkraftanlagen ausgewiesenen Landesfläche",
                      "Tatsächlich erreicht", "Ziel der Koalition (ohne Zieljahr)", "Prozent")

fig_wind_areas_en = create_fig_wind_areas(wind_areas_data,
                      "Share of the national area designated for wind power plants",
                      "Actually reached", "Target of the coalition (without target year)",
                      "Percentage")

fig_wind_areas_fr = create_fig_wind_areas(wind_areas_data,
                      "Part de la superficie nationale attribuée aux éoliennes",
                      "Déjà attribuée", "Objectif de la coalition (sans année cible)",
                      "Pourcentage")

fig_wind_areas_de.write_html("docs/figures/wind_areas_de.html", include_plotlyjs="cdn")
fig_wind_areas_en.write_html("docs/figures/wind_areas_en.html", include_plotlyjs="cdn")
fig_wind_areas_fr.write_html("docs/figures/wind_areas_fr.html", include_plotlyjs="cdn")

#%% resshares ######################################################################################

rs_data          = pd.read_csv("docs/data/resshares.csv", sep = ",")
rs_data['date']  = pd.to_datetime(rs_data['date'], format='%Y') #'%d.%m.%Y'

# Create indexm
rs_data['X']     = rs_data.index

#%% trend 2017-2021
rs_reg1_data = rs_data[['date','X','actual_bsv']].dropna()
rs_reg1_data = rs_reg1_data[(rs_reg1_data['date'] >= '2017') & (rs_reg1_data['date'] <= '2021')]
rs_reg1_x    = np.array(rs_reg1_data['X']).reshape((-1, 1))
rs_reg1      = LinearRegression().fit(rs_reg1_x, rs_reg1_data['actual_bsv'])

predict1_rs = rs_data[['date','X','actual_bsv']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2021')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2017_2021_bsv'] = predict1_rs.loc[0,'actual_bsv'] + predict1_rs[
    'index'] * rs_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2017_2021_bsv']]

#%% merge
rs_data = rs_data.merge(predict1_rs, how='left', on='date')
#windoff_data = windoff_data.merge(predict2_windoff, how='left', on='date')

#%% create resshares figure

def create_resshares(data,title,legend_title_achieved,
                     reached_consumption,reached_production,
                     linear,trend_2017_2021,
                     goal,legislative_period,percentage,
                     only_legislative_period,til_2030,til_2050,
                     legend_title_trends,
                     legend_title_ariadne,ariadne_lead,ariadne_lead_points,ariadne_span):
    """
    Create res share figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09", fillcolor = "green", opacity = 0.1, 
                  layer = 'below', annotation_text = legislative_period,
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             mode = "markers", name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.0f}%',
                             legendgroup = 'misc'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'misc',
                             legendgrouptitle_text = " "
                             ))

    fig.add_trace(go.Bar(x = data['date'], y = data['actual_bsv'],
                         name = reached_consumption,
                         marker_color = '#ff7f0e',
                         hovertemplate = '%{y:.1f}%',
                         legendgroup = 'achieved',
                         legendgrouptitle_text = legend_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_nse'],
                         name = reached_production,
                         marker_color = '#d62728',
                         hovertemplate = '%{y:.1f}%',
                         visible='legendonly',
                         legendgroup = 'achieved'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2017_2021_bsv'],
                             name = trend_2017_2021,
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legendgroup = 'trends',
                             legendgrouptitle_text = legend_title_trends))

    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead,
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.0f}%', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.0f}%', visible='legendonly',
                             legendgroup="ariadne"))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             ))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=only_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, 100]}]),
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2009-06", "2031-03"],
                            'yaxis.range': [0, 100]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2009-06", "2046-03"],
                            'yaxis.range': [0, 100]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = percentage, range = [0,100])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2009-06","2031-03"],
                     tickvals = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042,
                                 2043, 2044, 2045],
                     ticktext = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021*', '2022*', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040', '2041',
                                '2042', '2043', '2044', '2045'])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {'traceorder':'grouped', 'orientation':'h',
                                      'x': 0.0, 'y': -0.5,
                                      'yanchor': 'bottom', 'xanchor': 'left',
                                      'font': {'size': 13, 'color': '#000000'}},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_rs_de = create_resshares(rs_data, "Anteil erneuerbarer Energien im Stromsektor",
                             "Tatsächlich erreichter Anteil",
                             "Bruttostromverbrauch",
                             "Nettostromerzeugung","Linearer Verlauf",
                             "2017-2021 (Anteil Bruttostromverbrauch)",
                             "Ziel der Koalition", "Legislaturperiode", "Prozent",
                             "Nur Legislaturperiode","bis 2030","bis 2045",
                             "Historische Trends",
                             "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                             "Szenariokorridor")

fig_rs_en = create_resshares(rs_data, "Share of renewable energy in the power sector",
                             "Actually achieved share",                    
                             "Gross electricity consumption (GEC)",
                             "Net electricity generation",
                             "Linear progression",
                             "2017-2021 (share of GEC)",
                             "Target of the coalition","Legislative period","Percentage",
                             "Legislative period only","until 2030","until 2045",
                             "Historic trends",
                             "Ariadne scenarios","Lead model","Lead model (interpolated)",
                             "Scenario corridor")

fig_rs_fr = create_resshares(rs_data,
                             "Part de l'énergie renouvelable dans le secteur de l'électricité",
                             "Part d'électricité atteinte ",
                             "Consommation brute (CB)",
                             "Production nette",
                             "Progression linéaire",
                             "2017-2021 (CB)",
                             "Objectif de la coalition","Période du mandat","Pourcentage",
                             "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
                             "Tendances historiques",
                             "Scénarios Ariadne","Modèle de référence",
                             "Modèle de référence (tendance)",
                             "Intervalle de scénarios")

fig_rs_de.write_html("docs/figures/resshares_de.html", include_plotlyjs="cdn")
fig_rs_en.write_html("docs/figures/resshares_en.html", include_plotlyjs="cdn")
fig_rs_fr.write_html("docs/figures/resshares_fr.html", include_plotlyjs="cdn")

##
#%% heat pump data #######################################################################################

hp_data           = pd.read_csv("docs/data/hp.csv", sep = ",")
hp_data['date']   = pd.to_datetime(hp_data['date'], format='%Y')

# Create index
hp_data['X']          = hp_data.index

#%% trend 2017-2021
hp_reg1_data = hp_data[['date','X','actual']].dropna()
hp_reg1_data = hp_reg1_data[(hp_reg1_data['date'] >= '2017') &
                              (hp_reg1_data['date'] <= '2021')]
hp_reg1_x    = np.array(hp_reg1_data['X']).reshape((-1, 1))
hp_reg1      = LinearRegression().fit(hp_reg1_x, hp_reg1_data['actual'])

predict1_hp = hp_data[['date','X','actual']]
predict1_hp = predict1_hp[(predict1_hp['date'] >= '2021')].reset_index()
predict1_hp['index'] = predict1_hp.index
predict1_hp['trend_since_2017'] = predict1_hp.loc[0,'actual'] + predict1_hp['index'] * hp_reg1.coef_
predict1_hp = predict1_hp[['date','trend_since_2017']]

#%% merge
hp_data = hp_data.merge(predict1_hp, how='left', on='date')

#%% create heat pump figure

def create_hp_fig(data,title,legislative_period,reached,trend_2017_2021,
                   linear_trend,target_coalition,
                   only_legislative_period,til_2030,til_2050,
                   legend_title_trends,
                   legend_title_ariadne,ariadne_lead,ariadne_lead_points,ariadne_span,
                   legend_title_other_scenarios,agora,bdi,dena,bmwk,
                   y_axis_title,
                   y_max_2025,y_max_2030,y_max_2050):
    """
    Create heat pump figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09", fillcolor = "green", opacity = 0.1,
                     layer = 'below', annotation_text = legislative_period,
                     annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                            name = linear_trend, line = dict(color="#1f77b4", dash='dash'),
                            hovertemplate = '%{y:.3s}',
                            legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = target_coalition,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "#2ca02c",
                             hovertemplate = '%{y:.3s}',
                             legendgroup="misc"))

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'], name = reached,
                         marker_color='#ff7f0e', 
                         hovertemplate = '%{y:.3s}',
                         legendgroup="misc",
                         legendgrouptitle_text = " "))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead,
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="ariadne"))

    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             ))

    fig.add_trace(go.Scatter(x = data['date'], y = data['agora_knd2045'],
                             name = agora,
                             line = dict(color="#9010E6", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="others",
                             legendgrouptitle_text = legend_title_other_scenarios))
    fig.add_trace(go.Scatter(x = data['date'], y = data['bdi_klimapfade2'],
                             name = bdi,
                             line = dict(color="#353436", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="others"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['dena_kn100'],
                             name = dena,
                             line = dict(color="#F78902", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="others"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['bmwk_lfs_tnstrom'],
                             name = bmwk,
                             line = dict(color="#F7E702", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="others"))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=only_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, y_max_2025]}]),
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2010-06", "2031-03"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2010-06", "2046-06"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2010-06","2031-03"],
                     tickvals = [2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042,
                                 2043, 2044, 2045],
                     ticktext = ['2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021*', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040', '2041',
                                '2042', '2043', '2044', '2045'])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=120, b=0, pad=0),
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'h', 'x': 0.0, 'y': -0.6,
                  'yanchor': 'bottom', 'xanchor': 'left'})

    return fig

fig_hp_de = create_hp_fig(hp_data,"Bestand an Wärmpepumpen",
                            "Legislaturperiode",
                            "Tatsächlich erreicht","2017-2021",
                            "Linearer Verlauf", "Ziel der Koalition",
                            "Nur Legislaturperiode","bis 2030","bis 2045",
                            "Historische Trends",
                            "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                            "Szenariokorridor","Andere Szenarien (interpoliert)","Agora KND 2045","BDI Klimapfade 2.0","Dena KN100","BMWK LFS TN-Strom",
                            "Millionen Geräte",
                            5,8,20)

fig_hp_en = create_hp_fig(hp_data,"Stock of heat pumps",
                            "Legislative period",
                            "Actually reached","2017-2021",
                            "Linear progression","Target of the coalition",
                            "Legislative period only","until 2030","until 2045",
                            "Historic trends",
                            "Ariadne scenarios","Lead model","Lead model (interpolated)",
                            "Scenario corridor","Other scenarios (interpolated)","Agora KND 2045","BDI Klimapfade 2.0","Dena KN100","BMWK LFS TN-Strom",
                            "Million units",
                            5,8,20)

fig_hp_fr = create_hp_fig(hp_data,"Parc de pompes à chaleur",
                            "Période du mandat",
                            "Installées","2017-2021",
                            "Progression linéaire","Objectif de la coalition",
                            "Période du mandat", "Jusqu'en 2030", "Jusqu'en  2045",
                            "Tendances historiques",
                            "Scénarios Ariadne","Modèle de référence",
                            "Modèle de référence (tendance)",
                            "Intervalle de scénarios","Autres scénarios (tendance)","Agora KND 2045","BDI Klimapfade 2.0","Dena KN100","BMWK LFS TN-Strom",
                            "Unités (en millions)",
                            5,8,20)

fig_hp_de.write_html("docs/figures/hp_de.html", include_plotlyjs="cdn")
fig_hp_en.write_html("docs/figures/hp_en.html", include_plotlyjs="cdn")
fig_hp_fr.write_html("docs/figures/hp_fr.html", include_plotlyjs="cdn")



##

#%% resshare_heat ##################################################################################

rs_heat_data = pd.read_csv("docs/data/resshare_heat.csv", sep = ",")
rs_heat_data['date']  = pd.to_datetime(rs_heat_data['date'], format='%Y') #'%d.%m.%Y'

# Create index
rs_heat_data['X']          = rs_heat_data.index

#%% trend 2017-2021
rs_heat_reg1_data = rs_heat_data[['date','X','actual']].dropna()
rs_heat_reg1_data = rs_heat_reg1_data[
    (rs_heat_reg1_data['date'] >= '2016') & (rs_heat_reg1_data['date'] <= '2020')]
rs_heat_reg1_x    = np.array(rs_heat_reg1_data['X']).reshape((-1, 1))
rs_heat_reg1      = LinearRegression().fit(rs_heat_reg1_x, rs_heat_reg1_data['actual'])

predict1_rs = rs_heat_data[['date','X','actual']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2020')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2016_2020'] = predict1_rs.loc[0,'actual'] + predict1_rs[
    'index'] * rs_heat_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2016_2020']]

#%% merge
rs_heat_data = rs_heat_data.merge(predict1_rs, how='left', on='date')

#%% create heat_share figure

def create_rs_heat(data,title,reached,linear,trend,goal,legislative_period,percentage,
                   entire_timespan,only_legislative_period):
    """
    Create heat share figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2020-08", x1 = "2025", fillcolor = "green", opacity = 0.1,
                  layer = 'below', annotation_text = legislative_period,
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "#2ca02c", hovertemplate = '%{y:.1f}%'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'), hovertemplate = '%{y:.1f}%'))
    
    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = reached,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.1f}%'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2016_2020'],
                             name = trend,
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly'))

    fig.update_layout(updatemenus=[dict(type="buttons", direction="left", active=0, showactive=True,
                                    pad={"r": 10, "t": 0},
        xanchor="left", yanchor="top", x = 0, y = 1.15,
        buttons=list([dict(label = entire_timespan, method="relayout", args=[{
            'xaxis.autorange': True,
            'yaxis.autorange': True}]),
                      dict(label = only_legislative_period, method="relayout", args=[{
                          'xaxis.range': ["2020-07", "2026-03"],
                          'yaxis.range': [0, 40]}])]),)])

    fig.update_yaxes(matches = None, title = percentage)
    fig.update_xaxes(title=None, tickmode = 'array',
                    tickvals = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030],
                    ticktext = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030'])
    fig.update_layout(title = title,
                      template = "simple_white", uniformtext_mode = 'hide',
                      font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      legend_title_text = '',
                      legend = dict(
                          yanchor="bottom", xanchor="left", x=0, y=-.2, orientation = "h"),
                      margin=dict(l=0, r=0, t=130, b=0, pad=0),
                      xaxis_hoverformat='%Y',
                      xaxis_tickformat='%Y',
                      hovermode = "x")

    return fig

fig_rs_heat_de = create_rs_heat(rs_heat_data,"Anteil erneuerbarer Energien an der Wärmeerzeugung",
                                "Tatsächlich erreichter Anteil","Linearer Verlauf",
                                "Trend 2016-2020","Ziel der Koalition",
                                "Legislaturperiode","Prozent",
                                "Gesamter Zeitraum","Nur Legislaturperiode")
fig_rs_heat_en = create_rs_heat(rs_heat_data,"Share of renewable energy in heat supply",
                                "Share actually achieved","Linear progression",
                                "Trend 2016-2020","Target of the coalition",
                                "Legislative period","Percentage",
                                "Entire timespan","Legislative period only")
fig_rs_heat_fr = create_rs_heat(rs_heat_data,
                                "Part d'énergies renouvelables dans la fourniture de chaleur",
                                "Part atteinte","Progression linéaire",
                                "Tendance 2016-2020","Objectif de la coalition",
                                "Période du mandat","Pourcentage",
                                "Période entière","Période du mandat")

fig_rs_heat_de.write_html("docs/figures/resshare_heat_de.html", include_plotlyjs="cdn")
fig_rs_heat_en.write_html("docs/figures/resshare_heat_en.html", include_plotlyjs="cdn")
fig_rs_heat_fr.write_html("docs/figures/resshare_heat_fr.html", include_plotlyjs="cdn")

#%% BEV data #######################################################################################

bev_data           = pd.read_csv("docs/data/bev.csv", sep = ",")
bev_data['months'] = bev_data['months'].astype(str)
bev_data['date']   = pd.to_datetime(bev_data['date'], format='%d.%m.%Y')

# Create index
bev_data['X']          = bev_data.index

#%% trend 2017-2021
bev_reg1_data = bev_data[['date','X','actual']].dropna()
bev_reg1_data = bev_reg1_data[(bev_reg1_data['date'] >= '2018-01') &
                              (bev_reg1_data['date'] <= '2021-09')]
bev_reg1_x    = np.array(bev_reg1_data['X']).reshape((-1, 1))
bev_reg1      = LinearRegression().fit(bev_reg1_x, bev_reg1_data['actual'])

predict1_bev = bev_data[['date','X','actual']]
predict1_bev = predict1_bev[(predict1_bev['date'] >= '2021-10')].reset_index()
predict1_bev['index'] = predict1_bev.index
predict1_bev['trend_since_2017'] = predict1_bev.loc[0,'actual'] + predict1_bev['index'] * bev_reg1.coef_
predict1_bev = predict1_bev[['date','trend_since_2017']]

#%% dynamic trend last 12 months
bev_reg2_data = bev_data[['date','actual','X']].dropna()
bev_reg2_data = bev_reg2_data.tail(12)
bev_reg2_x    = np.array(bev_reg2_data['X']).reshape((-1, 1))
bev_reg2      = LinearRegression().fit(bev_reg2_x, bev_reg2_data['actual'])

predict2_bev = bev_data[['date','X','actual']]
predict2_bev = predict2_bev[(predict2_bev['date'] >= bev_reg2_data.iloc[-1]['date'])].reset_index()
predict2_bev['index'] = predict2_bev.index
predict2_bev['trend_last_12months'] = predict2_bev.loc[0,'actual'] + predict2_bev[
    'index'] * bev_reg2.coef_
predict2_bev = predict2_bev[['date','trend_last_12months']]

#%% merge
bev_data = bev_data.merge(predict1_bev, how='left', on='date')
bev_data = bev_data.merge(predict2_bev, how='left', on='date')

#%% create BEV figure

def create_bev_fig(data,title,legislative_period,reached,trend_2017_2021,trend_12_months,
                   linear_trend,target_coalition,only_legislative_period,til_2030,til_2050,
                   legend_title_trends,
                   legend_title_ariadne,ariadne_lead,ariadne_lead_points,ariadne_span,
                   y_axis_title,
                   y_max_2025,y_max_2030,y_max_2050):
    """
    Create BEV figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09", fillcolor = "green", opacity = 0.1,
                     layer = 'below', annotation_text = legislative_period,
                     annotation_position = "top left")

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'], name = reached,
                         marker_color='#ff7f0e', 
                         hovertemplate = '%{y:.3s}',
                         legendgroup="misc",
                         legendgrouptitle_text = " "))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
                            name = trend_12_months, line = dict(color="#d62728", dash='dot'),
                            hovertemplate = '%{y:.3s}', visible='legendonly',
                            legendgroup="trends"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                            name = linear_trend, line = dict(color="#1f77b4", dash='dash'),
                            hovertemplate = '%{y:.3s}',
                            legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = target_coalition,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "#2ca02c",
                             hovertemplate = '%{y:.3s}',
                             legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead,
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="ariadne"))

    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             ))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=only_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, y_max_2025]}]),
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2018-01", "2031-03"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2018-01", "2046-06"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2018-01", "2031-03"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=120, b=0, pad=0),
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'h',
                  'x': 0.0, 'y': -0.5,
                  'yanchor': 'bottom', 'xanchor': 'left'})

    return fig

fig_bev_de = create_bev_fig(bev_data,"Bestand batterieelektrischer Pkw",
                            "Legislaturperiode",
                            "Tatsächlich erreicht","2018-2021","Letzte 12 Monate",
                            "Linearer Verlauf", "Ziel der Koalition",
                            "Nur Legislaturperiode","bis 2030","bis 2045",
                            "Historische Trends",
                            "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
                            "Szenariokorridor", "Anzahl",
                            20000000,30000000,60000000)

fig_bev_en = create_bev_fig(bev_data,"Battery electric passenger car fleet",
                            "Legislative period",
                            "Actually reached","2018-2021","Last 12 months",
                            "Linear development","Target of the coalition",
                            "Legislative period only","until 2030","until 2045",
                            "Historic trends",
                            "Ariadne scenarios","Lead model","Lead model (interpolated)",
                            "Scenario corridor", "Units",
                            20000000,30000000,60000000)

fig_bev_fr = create_bev_fig(bev_data,"Flotte de voitures particulières électriques à batterie",
                            "Période du mandat",
                            "En circulation","2018-2021","12 derniers mois",
                            "Progression linéaire","Objectif de la coalition",
                            "Période du mandat", "Jusqu'en  2030", "Jusqu'en  2045",
                            "Tendances historiques",
                            "Scénarios Ariadne","Modèle de référence",
                            "Modèle de référence (tendance)",
                            "Intervalle de scénarios", "Unités",
                            20000000,30000000,60000000)

fig_bev_de.write_html("docs/figures/bev_de.html", include_plotlyjs="cdn")
fig_bev_en.write_html("docs/figures/bev_en.html", include_plotlyjs="cdn")
fig_bev_fr.write_html("docs/figures/bev_fr.html", include_plotlyjs="cdn")

#%% BEV new admissions ##############################################################################

bev_adm_data           = pd.read_csv("docs/data/bev_adm.csv", sep = ",")
bev_adm_data['months'] = bev_adm_data['months'].astype(str)
bev_adm_data['date']   = pd.to_datetime(bev_adm_data['date'], format='%d.%m.%Y')
bev_adm_data['actual'] = bev_adm_data['actual_bev'] + bev_adm_data['actual_phev']


bev_adm_data['X']          = bev_adm_data.index

#%% trend 2018-2021
bev_adm_reg1_data = bev_adm_data[['date','X','actual']].dropna()
bev_adm_reg1_data = bev_adm_reg1_data[(bev_adm_reg1_data['date'] >= '2018-01') &
                            (bev_adm_reg1_data['date'] <= '2021-09')]
bev_adm_reg1_x    = np.array(bev_adm_reg1_data['X']).reshape((-1, 1))
bev_adm_reg1      = LinearRegression().fit(bev_adm_reg1_x, bev_adm_reg1_data['actual'])

predict1_bev_adm = bev_adm_data[['date','X','actual']]
predict1_bev_adm = predict1_bev_adm[(predict1_bev_adm['date'] >= '2021-10')].reset_index()
predict1_bev_adm['index'] = predict1_bev_adm.index
predict1_bev_adm['trend_since_2018'] = predict1_bev_adm.loc[0,'actual'] + predict1_bev_adm['index'] * bev_adm_reg1.coef_
predict1_bev_adm = predict1_bev_adm[['date','trend_since_2018']]

#%% dynamic trend last 12 months
bev_adm_reg2_data = bev_adm_data[['date','actual','X']].dropna()
bev_adm_reg2_data = bev_adm_reg2_data.tail(12)
bev_adm_reg2_x    = np.array(bev_adm_reg2_data['X']).reshape((-1, 1))
bev_adm_reg2      = LinearRegression().fit(bev_adm_reg2_x, bev_adm_reg2_data['actual'])

predict2_bev_adm = bev_adm_data[['date','X','actual']]
predict2_bev_adm = predict2_bev_adm[(predict2_bev_adm['date'] >= bev_adm_reg2_data.iloc[-1]['date'])].reset_index()
predict2_bev_adm['index'] = predict2_bev_adm.index
predict2_bev_adm['trend_last_12months'] = predict2_bev_adm.loc[0,'actual'] + predict2_bev_adm['index'] * bev_adm_reg2.coef_
predict2_bev_adm = predict2_bev_adm[['date','trend_last_12months']]

#%% merge
bev_adm_data = bev_adm_data.merge(predict1_bev_adm, how='left', on='date')
bev_adm_data = bev_adm_data.merge(predict2_bev_adm, how='left', on='date')

# %% create bev_adm figure

def create_bev_adm(data,title,legislative_period,
              group_title_achieved,bev_adm_bev,bev_adm_phev,
#              group_title_trends,trend_2018_2021,trend_12_months,
              button_legislative_period,button_til_2030,
              y_axis):

    """
    Create bev_adm figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Bar(x = data['date'], y = data['actual_bev'],
                         name = bev_adm_bev,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.r}%',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_phev'],
                         name = bev_adm_phev,
                         marker_color = '#d62728', hovertemplate = '%{y:.r}%',
                         legendgroup = 'actual'))

#    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2018'],
#                             name = trend_2018_2021, line = dict(color="#ff7f0e", dash='dot'),
#                             hovertemplate = '%{y:.3s}%', visible='legendonly',
#                             legendgroup="trends",
#                             legendgrouptitle_text = group_title_trends))
#    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
#                            name = trend_12_months, line = dict(color="#d62728", dash='dot'),
#                            hovertemplate = '%{y:.3s}%', visible='legendonly',
#                            legendgroup="trends"))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=button_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, 100]}]),
                dict(label=button_til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2018-01", "2031-03"],
                            'yaxis.range': [0, 100]}]),
                #dict(label=til_2050,
                #     method="relayout",
                #     args=[{'xaxis.range': ["2018-01", "2046-06"],
                #            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis, range = [0,100])
    fig.update_xaxes(title=None, range = ["2018-01", "2025-12"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                         legend = dict(yanchor="bottom", xanchor="left", x=0, y=-.4,
                                       orientation = "h"),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=130, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x")

    return fig

#def create_bev_adm(data,title,legislative_period,
#              group_title_achieved,bev_adm_normal,bev_adm_fast,
#              group_title_trends,trend_2017_2021,trend_12_months,
#              button_legislative_period,button_til_2030,
#              y_axis):

fig_bev_adm_de = create_bev_adm(bev_adm_data,"Anteile an den monatlichen Neuzulassungen","Legislaturperiode",
                      "Tatsächlich erreicht","Batterieelektrische Pkw","Plug-in-Hybrid-Pkw",
#                      "Historische Trends (Summe)","2018-2021","Letzte 12 Monate",
                      "Nur Legislaturperiode","bis 2030",
                      "Prozent")

fig_bev_adm_en = create_bev_adm(bev_adm_data,"Shares in monthly new registrations","Legislative period",
                      "Actually achieved","Battery-electric passenger cars","Plug-in hybrid passenger cars",
#                      "Historic trends (sum)","2018-2021","Last 12 months",
                      "Legislative period only","until 2030",
                      "Percent")
fig_bev_adm_fr = create_bev_adm(bev_adm_data,"Parts des nouvelles immatriculations mensuelles","Période du mandat",
                      "Réalisé","Voitures électriques à batterie","Voitures hybrides plug-in",
#                      "Tendances historiques (somme)","2018-2021","12 derniers mois",
                      "Période du mandat","Jusqu'en  2030",
                      "Pourcentage")

fig_bev_adm_de.write_html("docs/figures/bev_adm_de.html", include_plotlyjs="cdn")
fig_bev_adm_en.write_html("docs/figures/bev_adm_en.html", include_plotlyjs="cdn")
fig_bev_adm_fr.write_html("docs/figures/bev_adm_fr.html", include_plotlyjs="cdn")

#%% charging stations ##############################################################################

cs_data           = pd.read_csv("docs/data/cs.csv", sep = ",")
cs_data['months'] = cs_data['months'].astype(str)
cs_data['date']   = pd.to_datetime(cs_data['date'], format='%d.%m.%Y')
cs_data['actual'] = cs_data['actual_normal'] + cs_data['actual_fast']


cs_data['X']          = cs_data.index

#%% trend 2018-2021
cs_reg1_data = cs_data[['date','X','actual']].dropna()
cs_reg1_data = cs_reg1_data[(cs_reg1_data['date'] >= '2017-01') &
                            (cs_reg1_data['date'] <= '2021-09')]
cs_reg1_x    = np.array(cs_reg1_data['X']).reshape((-1, 1))
cs_reg1      = LinearRegression().fit(cs_reg1_x, cs_reg1_data['actual'])

predict1_cs = cs_data[['date','X','actual']]
predict1_cs = predict1_cs[(predict1_cs['date'] >= '2021-10')].reset_index()
predict1_cs['index'] = predict1_cs.index
predict1_cs['trend_since_2017'] = predict1_cs.loc[0,'actual'] + predict1_cs['index'] * cs_reg1.coef_
predict1_cs = predict1_cs[['date','trend_since_2017']]

#%% dynamic trend last 12 months
cs_reg2_data = cs_data[['date','actual','X']].dropna()
cs_reg2_data = cs_reg2_data.tail(12)
cs_reg2_x    = np.array(cs_reg2_data['X']).reshape((-1, 1))
cs_reg2      = LinearRegression().fit(cs_reg2_x, cs_reg2_data['actual'])

predict2_cs = cs_data[['date','X','actual']]
predict2_cs = predict2_cs[(predict2_cs['date'] >= cs_reg2_data.iloc[-1]['date'])].reset_index()
predict2_cs['index'] = predict2_cs.index
predict2_cs['trend_last_12months'] = predict2_cs.loc[0,'actual'] + predict2_cs['index'] * cs_reg2.coef_
predict2_cs = predict2_cs[['date','trend_last_12months']]

#%% merge
cs_data = cs_data.merge(predict1_cs, how='left', on='date')
cs_data = cs_data.merge(predict2_cs, how='left', on='date')

# %% create cs figure

def create_cs(data,title,legislative_period,
              goal,linear,
              group_title_achieved,cs_normal,cs_fast,
              group_title_trends,trend_2017_2021,trend_12_months,
              button_legislative_period,button_til_2030,
              y_axis):

    """
    Create CS figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers",
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.2r}',
                             legendgroup = 'misc',
                             legendgrouptitle_text = " "))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan'],
                             name = linear,
                             line = dict(color="#1f77b4", dash='dot'), hovertemplate = '%{y:.2r}',
                             legendgroup = 'misc'))

    fig.add_trace(go.Bar(x = data['date'], y = data['actual_normal'],
                         name = cs_normal,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.r}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_fast'],
                         name = cs_fast,
                         marker_color = '#d62728', hovertemplate = '%{y:.r}',
                         legendgroup = 'actual'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = group_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
                            name = trend_12_months, line = dict(color="#d62728", dash='dot'),
                            hovertemplate = '%{y:.3s}', visible='legendonly',
                            legendgroup="trends"))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=button_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, 500000]}]),
                dict(label=button_til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2017-01", "2031-03"],
                            'yaxis.range': [0, 1100000]}]),
                #dict(label=til_2050,
                #     method="relayout",
                #     args=[{'xaxis.range': ["2018-01", "2046-06"],
                #            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis, range = [0,1100000])
    fig.update_xaxes(title=None, range = ["2017-01", "2031-03"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                         legend = dict(yanchor="bottom", xanchor="left", x=0, y=-.4,
                                       orientation = "h"),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=130, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x")

    return fig

fig_cs_de = create_cs(cs_data,"Ladepunkte in Betrieb","Legislaturperiode",
                      "Ziel der Koalition","Linearer Verlauf",
                      "Tatsächlich erreicht","Normalladepunkte","Schnellladepunkte",
                      "Historische Trends (Summe)","2017-2021","Letzte 12 Monate",
                      "Nur Legislaturperiode","bis 2030",
                      "Anzahl")

fig_cs_en = create_cs(cs_data,"Charging points in operation","Legislative period",
                      "Target of the coalition","Linear progression",
                      "Actually achieved","Normal charging points","Fast charging points",
                      "Historic trends (sum)","2017-2021","Last 12 months",
                      "Legislative period only","until 2030",
                      "Units")
fig_cs_fr = create_cs(cs_data,"Points de charge en opération","Période du mandat",
                      "Objectif de la coalition","Progression linéaire",
                      "Réalisé","Points de charge normaux","Points de charge rapides",
                      "Tendances historiques (somme)","2017-2021","12 derniers mois",
                      "Période du mandat","Jusqu'en  2030",
                      "Unités")

fig_cs_de.write_html("docs/figures/cs_de.html", include_plotlyjs="cdn")
fig_cs_en.write_html("docs/figures/cs_en.html", include_plotlyjs="cdn")
fig_cs_fr.write_html("docs/figures/cs_fr.html", include_plotlyjs="cdn")


#%% BEV per charging point ##############################################################################

# Define bev_per_cp_data by using bev_data
bev_per_cp_data = bev_data

# Drop 2017 charging station data for which we don't have BEV stock data
cs_data = cs_data.loc[cs_data['date'] >= '15.01.2018']
cs_data = cs_data.reset_index(drop=True)
# Define variables of interest
bev_per_cp_data['normal'] = bev_data['actual'] / cs_data['actual_normal']
bev_per_cp_data['fast'] = bev_data['actual'] / cs_data['actual_fast']
bev_per_cp_data['total'] = bev_data['actual'] / (cs_data['actual_normal']+cs_data['actual_fast'])


bev_per_cp_data['X']          = bev_per_cp_data.index

# %% create bev_per_cp figure

def create_bev_per_cp(data,title,legislative_period,
              group_title_achieved,bev_per_normal,bev_per_fast,bev_per_total,
              button_legislative_period,button_til_2030,
              y_axis):

    """
    Create bev_per_cp figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['normal'],
                         name = bev_per_normal,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="#ff7f0e", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}',
                         legendgrouptitle_text = group_title_achieved))

    fig.add_trace(go.Scatter(x = data['date'], y = data['fast'],
                         name = bev_per_fast,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="#d62728", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'))
                        
    fig.add_trace(go.Scatter(x = data['date'], y = data['total'],
                         name = bev_per_total,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="black", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'))


    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=button_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-06", "2025-12"],
                            'yaxis.range': [0, 150]}]),
                dict(label=button_til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2018-01", "2031-03"],
                            'yaxis.range': [0, 150]}]),
                #dict(label=til_2050,
                #     method="relayout",
                #     args=[{'xaxis.range': ["2018-01", "2046-06"],
                #            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis, range = [0,150])
    fig.update_xaxes(title=None, range = ["2018-01", "2025-12"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                         legend = dict(yanchor="bottom", xanchor="left", x=0, y=-.4,
                                       orientation = "h"),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=130, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x")

    return fig

fig_bev_per_cp_de = create_bev_per_cp(bev_per_cp_data,"Batterieelektrische Fahrzeuge pro öffentlichem Ladepunkt","Legislaturperiode",
                      "Bisher erreicht","BEV pro Normalladepunkt","BEV pro Schnellladepunkt","BEV pro Ladepunkt (Schnell+Normal)",
                      "Nur Legislaturperiode","bis 2030",
                      "")
fig_bev_per_cp_en = create_bev_per_cp(bev_per_cp_data,"Battery electric cars per public charging point","Legislative period",
                      "Actually achieved","BEV per normal charging point","BEV per fast charging point","BEV per charging point (fast+normal)",
                      "Legislative period only","until 2030",
                      "")
fig_bev_per_cp_fr = create_bev_per_cp(bev_per_cp_data,"Voitures particulières électriques à batterie par borne de recharge","Période du mandat",
                      "Réalisé","BEV par borne de recharge normake","BEV par borne de recharge rapide","BEV par borne de recharge (rapide+normale)",
                      "Période du mandat","Jusqu'en  2030",
                      "")

fig_bev_per_cp_de.write_html("docs/figures/bev_per_cp_de.html", include_plotlyjs="cdn")
fig_bev_per_cp_en.write_html("docs/figures/bev_per_cp_en.html", include_plotlyjs="cdn")
fig_bev_per_cp_fr.write_html("docs/figures/bev_per_cp_fr.html", include_plotlyjs="cdn")


#%% railroad electrification data ##################################################################

rail_elec_data         = pd.read_csv("docs/data/electrification.csv", sep = ",")
rail_elec_data['date'] = pd.to_datetime(rail_elec_data['date'], format='%Y')
rail_elec_data['X']    = rail_elec_data.index

#%% trend 2017-2021
rail_elec1_data = rail_elec_data[['date','X','actual']].dropna()
rail_elec1_data = rail_elec1_data[(rail_elec1_data['date'] >= '2017') &
                                  (rail_elec1_data['date'] <= '2021')]
rail_elec_reg1_x    = np.array(rail_elec1_data['X']).reshape((-1, 1))
rail_elec_reg1      = LinearRegression().fit(rail_elec_reg1_x, rail_elec1_data['actual'])
predict1_rail_elec = rail_elec_data[['date','X','actual']]
predict1_rail_elec = predict1_rail_elec[(predict1_rail_elec['date'] >= '2021')].reset_index()
predict1_rail_elec['index'] = predict1_rail_elec.index
predict1_rail_elec['trend_since_2017'] = predict1_rail_elec.loc[0,'actual'] + predict1_rail_elec['index'] * rail_elec_reg1.coef_
predict1_rail_elec = predict1_rail_elec[['date','trend_since_2017']]

#%% dynamic trend last 12 months
rail_elec_reg2_data = rail_elec_data[['date','actual','X']].dropna()
rail_elec_reg2_data = rail_elec_reg2_data.tail(12)
rail_elec_reg2_x    = np.array(rail_elec_reg2_data['X']).reshape((-1, 1))
rail_elec_reg2      = LinearRegression().fit(rail_elec_reg2_x, rail_elec_reg2_data['actual'])

predict2_rail_elec = rail_elec_data[['date','X','actual']]
predict2_rail_elec = predict2_rail_elec[(predict2_rail_elec['date'] >= rail_elec_reg2_data.iloc[-1]['date'])].reset_index()
predict2_rail_elec['index'] = predict2_rail_elec.index
predict2_rail_elec['trend_last_12months'] = predict2_rail_elec.loc[0,'actual'] + predict2_rail_elec['index'] * rail_elec_reg2.coef_
predict2_rail_elec = predict2_rail_elec[['date','trend_last_12months']]

#%% merge
rail_elec_data = rail_elec_data.merge(predict1_rail_elec, how='left', on='date')
rail_elec_data = rail_elec_data.merge(predict2_rail_elec, how='left', on='date')

#%% create railroad electrification figure

def create_rail(data,title,legislative_period,
                goal,linear,
                actual,
                group_title_trends,trend_2017_2021,trend_12_months,
                y_axis):
    
    """
    Create rail figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "#2ca02c",
                             hovertemplate = '%{y:.3s}%',
                             legendgroup = 'misc',
                             legendgrouptitle_text = " "))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan'],
                         name = linear,
                         mode = 'lines', line = dict(color="#1f77b4", dash='dot'),
                         hovertemplate = '%{y:.3s}%',
                         legendgroup = 'misc'))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = actual,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.3s}%',
                         legendgroup = 'misc'))

    fig.add_vrect(x0 = "2021-05", x1 = "2025-09",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="trends", mode = 'lines',
                             legendgrouptitle_text = group_title_trends))
    #fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
    #                        name = trend_12_months, mode = 'lines',
    #                        line = dict(color="#d62728", dash='dot'),
    #                        hovertemplate = '%{y:.3s}', visible='legendonly',
    #                        legendgroup="trends"))

    fig.update_yaxes(matches = None, title = y_axis)
    fig.update_xaxes(title=None)

    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                      font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      legend = dict(yanchor="bottom", xanchor="left", x=0, y=-.4, orientation="h"),
                      legend_title_text = '', margin=dict(l=0, r=0, t=130, b=0, pad=0),
                      xaxis_hoverformat='%Y', hovermode = "x",
                      legend_groupclick = "toggleitem")

    return fig

fig_rail_elec_de = create_rail(rail_elec_data, "Elektrifizierter Anteil des Schienennetzes","Legislaturperiode",
                               "Ziel der Koalition","Linearer Verlauf","Tatsächlich erreicht",
                               "Historische Trends","2017-2021","Letzte 12 Monate",
                               "Prozent")
fig_rail_elec_en = create_rail(rail_elec_data, "Electrified share of the rail network","Legislative period",
                               "Target of the coalition","Linear progression","Actually achieved",
                               "Historic trends","2017-2021","Last 12 months",
                               "Percentage")
fig_rail_elec_fr = create_rail(rail_elec_data, "Part du réseau ferroviaire électrifié","Période du mandat",
                               "Objectif de la coalition","Progression linéaire","Réalisé",
                               "Tendances historiques","2017-2021","12 derniers mois",
                               "Pourcentage")

fig_rail_elec_de.write_html("docs/figures/electrification_de.html", include_plotlyjs="cdn")
fig_rail_elec_en.write_html("docs/figures/electrification_en.html", include_plotlyjs="cdn")
fig_rail_elec_fr.write_html("docs/figures/electrification_fr.html", include_plotlyjs="cdn")


#%% electrolysis ###################################################################################

h2_data           = pd.read_csv("docs/data/h2.csv", sep = ",")
h2_data['months'] = h2_data['months'].astype(str)
h2_data['date']   = pd.to_datetime(h2_data['date'], format='%d.%m.%Y')

#%% create electrolysis figure

def create_h2(data,title,actual,linear,goal,legislative_period,
              y_axis_title,
              only_legislative_period,til_2030,til_2050,
              ariadne_lead,legend_title_ariadne,ariadne_lead_points,ariadne_span,
              y_max_2025,y_max_2030,y_max_2050):
    """
    Create H2 figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-10", x1 = "2025-09",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = actual,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup = 'misc',
                         legendgrouptitle_text = ' '))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                         name = linear,
                         mode = 'lines', line = dict(color="#1f77b4", dash='dot'),
                         hovertemplate = '%{y:.2f}GW',
                         legendgroup = 'misc'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup = 'misc'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead,
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne"))

    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             )) 

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=only_legislative_period,
                     method="relayout",
                     args=[{'xaxis.range': ["2021-07", "2025-12"],
                            'yaxis.range': [0, y_max_2025]}]),
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2020-12", "2031-03"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2020-12", "2046-06"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_xaxes(title=None, range = ["2020-12", "2031-03"])
    fig.update_yaxes(matches = None, title = y_axis_title, range = [0, y_max_2030])

    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      legend_title_text = '', margin=dict(l=0, r=0, t=130, b=0, pad=0),
                      xaxis_hoverformat='%Y', hovermode = "x",
                      legend_groupclick = "toggleitem",
                      legend = {'traceorder':'grouped', 'orientation':'h',
                                'x': 0.0, 'y': -0.5,
                                'yanchor': 'bottom', 'xanchor': 'left'})

    return fig

fig_h2_de = create_h2(h2_data, "Installierte Leistung Elektrolyse",
                      "Tatsächlich erreicht", "Linearer Verlauf","Ziel der Koalition",
                      "Legislaturperiode","Gigawatt",
                      "Nur Legislaturperiode","bis 2030","bis 2045",
                      "Ariadne-Szenarien","Leitmodell",
                      "Leitmodell (interpoliert)","Szenariokorridor",
                      6,15,50)
fig_h2_en = create_h2(h2_data, "Installed electrolysis capacity",
                      "Actually achieved", "Linear progression","Target of the coalition",
                      "Legislative period","Gigawatt",
                      "Legislative period only","until 2030","until 2045",
                      "Ariadne scenarios","Lead model",
                      "Lead model (interpolated)","Scenario corridor",
                      6,15,50)
fig_h2_fr = create_h2(h2_data, "Capacité installée d'électrolyse",
                      "Déjà installée", "Progression linéaire","Objectif de la coalition",
                      "Période du mandat","Gigawatt",
                      "Période du mandat", "Jusqu'en  2030", "Jusqu'en  2045",
                      "Scénarios Ariadne","Modèle de référence",
                      "Modèle de référence (tendance)","Intervalle de scénarios",
                      6,15,50)

fig_h2_de.write_html("docs/figures/h2_de.html", include_plotlyjs="cdn")
fig_h2_en.write_html("docs/figures/h2_en.html", include_plotlyjs="cdn")
fig_h2_fr.write_html("docs/figures/h2_fr.html", include_plotlyjs="cdn")

# %% gas imports

gas_data           = pd.read_csv("docs/data/gas.csv", sep = ",")
gas_data['date']   = pd.to_datetime(gas_data['date'], format='%d.%m.%Y') #'%d.%m.%Y'

#%% create gas figure

def create_gas_figure(data,title,net_imports,ru_nordstream,ru_poland,ru_cz,norway,netherlands,
                       other_import,overall_import, net_exports, pl_exports, cz_export,
                       other_export):
    """
    Create gas import figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_nordstream'],
                             name = ru_nordstream,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .80)",
                             line=dict(width=1, color = 'rgba(26, 55, 77, 1)'),
                             legendgroup="imports",
                             legendgrouptitle_text=net_imports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_poland'],
                             name = ru_poland,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .60)",
                             line=dict(width=1, color = "rgba(26, 55, 77, .80)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_cz'],
                             name = ru_cz,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .40)",
                             line=dict(width=1, color = "rgb(26, 55, 77, .60)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['norway'],
                             name = norway,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(205, 182, 153, 0.5)",
                             line=dict(width=0, color = "rgba(205, 182, 153, 0.75)"),
                            legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['netherlands'],
                             name = netherlands,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(187, 100, 100, 0.5)",
                             line=dict(width=0, color = "rgba(187, 100, 100, 0.75)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['others_net_imports'],
                             name = other_import,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.15)",
                             line=dict(width=0, color = "rgba(180, 207, 176, .75)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['pl_net_exports'],
                             name = pl_exports,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.50)",
                             line=dict(width=0, color = "rgba(64, 104, 130, .75)"),
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['cz_net_exports'],
                             name = cz_export,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.30)",
                             line=dict(width=0, color = "rgba(255, 211, 101, .75)"),
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['others_net_exports'],
                             name = other_export,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.15)",
                             line=dict(width=0, color = "rgba(180, 207, 176, 0.75)"),
                             legendgroup="exports",
                             hovertemplate = '%{y:.2f}'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['total_net_imports'],
                             name = overall_import,
                             mode = 'lines',
                             line_shape="spline",
                             line = dict(color="black", dash = "solid", width=3),
                             #legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))

    fig.update_yaxes(matches = None, title = 'TWh', range = [-50,150])
    fig.update_layout(title = title, template = "simple_white",
                     uniformtext_mode = 'hide', legend_title_text = '',
                     font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                     margin=dict(l=0, r=0, t=100, b=0, pad=0),
                     xaxis_hoverformat='%b %Y',
                     hovermode = "x unified",
                     hoverlabel = dict(namelength = -1),
                     legend_groupclick = "toggleitem",
                     legend_traceorder="reversed",
                     legend = {'traceorder':'grouped', 'orientation':'h',
                               'x': 0.0, 'y': -0.7,
                               'yanchor': 'bottom', 'xanchor': 'left'
                     })
    return fig

gas_figure_de = create_gas_figure(gas_data,"Netto-Erdgasimporte nach Deutschland",
                                   "Netto-Importe",
                                   "Russland (via Nord Stream 1)","Russland (via Polen)",
                                   "Russland (via Tschechien)","Norwegen","Niederlande","Andere",
                                   "Gesamt",
                                   "Netto-Exporte","Polen","Tschechien","Andere")

gas_figure_en = create_gas_figure(gas_data,"Net natural gas imports to Germany",
                                   "Net imports",
                                   "Russia (via Nord Stream 1)","Russia (via Poland)",
                                   "Russia (via Czech Republic)","Norway","Netherlands","Others",
                                   "Total",
                                   "Net exports","Poland","Czech Republic","Others")

gas_figure_fr = create_gas_figure(gas_data,"Importations nettes de gaz naturel en Allemagne",
                                   "Importations nettes",
                                   "Russie (par Nord Stream 1)","Russie (via la Pologne)",
                                   "Russie (par la République Tchèque)","Norvège","Pays-Bas",
                                   "Autres",
                                   "Total",
                                   "Exportations nettes","Pologne","République Tchèque","Autres")

gas_figure_de.write_html("docs/figures/gas_de.html", include_plotlyjs="cdn")
gas_figure_en.write_html("docs/figures/gas_en.html", include_plotlyjs="cdn")
gas_figure_fr.write_html("docs/figures/gas_fr.html", include_plotlyjs="cdn")

#%%
