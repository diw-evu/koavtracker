---
description: The KoaVTracker visualises the goals and progress of the new government coalition in Germany for selected areas of the energy transition.

hide:
  - navigation
  - toc
---

# #KoaVTracker

!!! danger "New in version 0.5.3"
    
    The KoaVtracker now also contains heat pump data, monthly data on electric passenger car registrations, and data on the relation of electric cars and charging points. It further shows selected data from the [scenario analysis](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) for comparison.

## Energy targets in the coalition agreement of the German government vs. where we stand today

In their [coalition agreement](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) of November 24, 2021, the parties of the so-called "traffic light" coalition set themselves various quantitative targets for the German energy sector. On this webpage, we provide graphical illustrations of these targets, which are mostly specified for the year 2030, and compare them to the actual status achieved today. We initially focus on targets for the expansion of renewable energy capacity, electric mobility and hydrogen. For some of these indicators, data is available and updated on a monthly basis; for others, only annual data is available. For purely illustrative reasons, a linear time path is shown for each indicator between the current status and the target year (usually December 2030). The exact paths were not specified in the coalition agreement. For some indicators, we also include projections based on recent trends. In the future, we plan to add more indicators and time trends.

The coalition's goals and previous trajectories can also be compared with the results of the [Ariadne scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) for some indicators. These are scenario analyses carried out in the BMBF-funded Copernicus project [Ariadne](https://ariadneprojekt.de/) and show different paths to climate neutrality for Germany in 2045. We present both the corridors spanned by all Ariadne scenarios and the results of the respective Ariadne lead model in the so-called *Technology Mix Scenario* (8Gt_Bal). In the Ariadne scenarios, data are generally only available in five-year steps (2020, 2025, 2030, ...). We have interpolated linearly between these base years. The data of the Ariadne scenarios are available until 2045, and thus allow a longer-term view of plausible developments of the respective indicators. Further visualisations and data on all Ariadne scenarios are available on external websites in the [Pathfinder](https://pathfinder.ariadneprojekt.de/) and in the [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne). Later, we will add more indicators or time courses if necessary.

We call this tool "KoaVTracker". "KoaV" stand for *Koalitionsvertrag*, which means coalition agreement; and "Tracker" - well, we like anglicisms.

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## About us

This page was created by [Adeline Guéret](https://www.diw.de/cv/en/agueret), [Alexander Roth](https://www.diw.de/cv/en/aroth) and [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), members of the research area [Transformation of the Energy Economy](https://twitter.com/transenerecon) in the Department [Energy, Transport, Environment](https://www.diw.de/en/diw_01.c.604205.en/energy__transportation__environment_department.html) at [DIW Berlin](https://www.diw.de/en) in the context of the BMBF-funded Copernicus Project [Ariadne](https://ariadneprojekt.de/). If you have any questions, please contact [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill).
